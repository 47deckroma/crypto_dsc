import com.adobe.consulting.crypto.CAdESSigner;
import com.adobe.consulting.signature.P7M;
import com.adobe.consulting.signature.ProtocolTypes;
import com.adobe.consulting.signature.TSLUpdateException;
import com.adobe.consulting.signature.VerifyException;
import com.adobe.consulting.types.P7MVerifyResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import org.bouncycastle.ocsp.OCSPException;

public class testP7M
{
  public static void main(String[] args)
    throws VerifyException, OCSPException, TSLUpdateException
  {
    P7M p7m = new P7M();
    FileInputStream fis = null;
    try
    {
      fis = new FileInputStream(new File("c:/Cert/rinnovo_RCT2019firmato.pdf.p7m"));
      CAdESSigner signer = new CAdESSigner();
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }

    P7MVerifyResult result = p7m.verifyP7M(fis, "C:/Cert/", "CAanchorsStore", false, ProtocolTypes.Http,true);

    System.out.println("STATUS: " + result.getVerifySignatureResult());
  }
}