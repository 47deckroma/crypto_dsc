import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.security.CertificateInfo;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import java.io.IOException;
import java.io.PrintStream;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Calendar;

public class testSignatures
{
  public static void main(String[] args)
    throws IOException
  {
    PdfReader reader = new PdfReader("/Users/erivelli/Desktop/Telepass_campo_firma_livecycle.pdf");
    AcroFields af = reader.getAcroFields();
    ArrayList<String> names = af.getBlankSignatureNames();
    for (String name : names) {
      System.out.println("Signature name: " + name);
      System.out.println("Signature covers whole document: " + af.signatureCoversWholeDocument(name));
      System.out.println("Document revision: " + af.getRevision(name) + " of " + af.getTotalRevisions());
      PdfPKCS7 pk = af.verifySignature(name);
      Calendar cal = pk.getSignDate();
      Certificate[] pkc = pk.getCertificates();
      System.out.println("Subject: " + CertificateInfo.getSubjectFields(pk.getSigningCertificate()));
    }
  }
}