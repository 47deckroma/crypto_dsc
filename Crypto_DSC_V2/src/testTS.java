import java.io.FileInputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.tsp.TimeStampTokenInfo;

public class testTS
{
  public static void main(String[] args)
  {
    try
    {
      FileInputStream inresp = new FileInputStream("/Users/erivelli/Desktop/a6af6dab-037b-4b1a-ab27-977a62726198_TSAResponse-4.TSARES");

      TimeStampResponse resp = new TimeStampResponse(inresp);

      System.out.println("TimeStamp verified.");
      TimeStampToken tsToken = resp.getTimeStampToken();
      TimeStampTokenInfo tsInfo = tsToken.getTimeStampInfo();
      SignerId signer_id = tsToken.getSID();
      BigInteger cert_serial_number = signer_id.getSerialNumber();
      System.out.println("Generation time " + tsInfo.getGenTime());
      System.out.println("Signer ID serial " + signer_id.getSerialNumber());
      System.out.println("Signer ID issuer " + signer_id.getIssuer());
    } catch (TSPException tsex) {
      System.out.println(tsex.getMessage());
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}