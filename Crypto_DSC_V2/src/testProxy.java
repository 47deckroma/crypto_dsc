import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;

import com.adobe.consulting.signature.MyAuthenticator;


public class testProxy {
	
	
	public static void main(String[] args) {
		testProxy(true,"http://crl.arubapec.it/ArubaPECSpACertificationAuthorityC/LatestCRL.crl","yrui002","tar88get","itaca-prod.utenze.bankit.it","8080" );
	}
		
	
	public static void testProxy(boolean useProxy,String url,String proxyUsr,String proxyPwd,String proxyHost,String proxyPort){
		InputStream crlInputStream = null;
		URLConnection conn = null;
		try
		{
			URL urla = new URL(url);
			if (useProxy) {
				Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));
				SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));
				Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
				conn = urla.openConnection(proxy);
			} else {
				conn = urla.openConnection();
			}
			System.out.println("Type:"+conn.getContentType());
			System.out.println("Length:"+conn.getContentLength());
			crlInputStream = conn.getInputStream();
			System.out.println(crlInputStream);
		}
		catch (MalformedURLException e1)
		{
			e1.printStackTrace();
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}catch ( Exception e1) {
			e1.printStackTrace();
		}finally{
			 
		}
	}

}
