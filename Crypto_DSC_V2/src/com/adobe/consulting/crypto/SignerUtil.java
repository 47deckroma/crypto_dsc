package com.adobe.consulting.crypto;

public class SignerUtil
{
  public static String asHex(byte[] buf)
  {
    StringBuffer strbuf = new StringBuffer(buf.length * 2);

    for (int i = 0; i < buf.length; i++)
    {
      if ((buf[i] & 0xFF) < 16)
        strbuf.append("0");
      strbuf.append(Long.toString(buf[i] & 0xFF, 16));
    }
    return strbuf.toString();
  }
}