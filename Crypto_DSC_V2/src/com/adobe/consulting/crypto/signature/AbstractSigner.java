package com.adobe.consulting.crypto.signature;

import com.adobe.consulting.crypto.SignerUtil;
import com.adobe.consulting.crypto.bean.ValidationInfos;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CRL;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import javax.crypto.Cipher;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.cmp.PKIStatus;
import org.bouncycastle.asn1.cmp.PKIStatusInfo;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.tsp.TimeStampResp;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Arrays;

public abstract class AbstractSigner
{
  Logger log = Logger.getLogger(AbstractSigner.class);
  protected File file;
  protected List<File> detachedFiles;
  protected TimeStampToken[] timestamptokens = null;

  private boolean base64 = false;

  public boolean isBase64() {
    return this.base64;
  }

  public void setBase64(boolean base64) {
    this.base64 = base64;
  }

  public abstract SignerType getFormat();

  public abstract boolean isSignedType(File paramFile);

  public abstract TimeStampToken[] getTimeStampTokens();

  public void reset()
  {
    this.timestamptokens = null;
  }

  public abstract InputStream getUnsignedContent();

  public abstract boolean canContentBeSigned();

  public abstract byte[] getUnsignedContentDigest(MessageDigest paramMessageDigest);

  public abstract List<ISignature> getSignatures();

  public abstract Collection<CRL> getEmbeddedCRLs();

  public abstract Collection<? extends Certificate> getEmbeddedCertificates();

  public File getFile()
  {
    return this.file;
  }

  public void setFile(File file)
  {
    this.file = file;
  }

  public ValidationInfos validateTimeStampTokensEmbedded()
    throws CMSException
  {
    return null;
  }

  public ValidationInfos validateTimeStampTokensDetached(File attachedFile)
  {
    return null;
  }

  protected void checkTimeStampTokenOverRequest(ValidationInfos validationInfos, TimeStampToken timestamptoken, TimeStampRequest request)
  {
    try
    {
      PKIStatusInfo paramPKIStatusInfo = new PKIStatusInfo(PKIStatus.granted);

      ASN1InputStream aIn = new ASN1InputStream(timestamptoken.getEncoded());
      ASN1Sequence seq = (ASN1Sequence)aIn.readObject();
      ContentInfo paramContentInfo = new ContentInfo(seq);
      TimeStampResp tsr = new TimeStampResp(paramPKIStatusInfo, paramContentInfo);
      TimeStampResponse response = new TimeStampResponse(tsr);

      checkTimeStampRequestOverTimeStampResponse(validationInfos, timestamptoken, request, response);
    }
    catch (IOException e) {
      validationInfos.addError("Il token non contiene una marca temporale valida");
    } catch (TSPException e) {
      validationInfos.addError(e.getMessage());
    }
  }

  private void checkTimeStampRequestOverTimeStampResponse(ValidationInfos validationInfos, TimeStampToken timestamptoken, TimeStampRequest request, TimeStampResponse response)
  {
    String digestAlgorithmOID = null;
    try
    {
      response.validate(request);
    } catch (TSPException e) {
      validationInfos.addError(e.getMessage());
    }

    try
    {
      CMSSignedData cms = timestamptoken.toCMSSignedData();
      CertStore certStore = timestamptoken.getCertificatesAndCRLs("Collection", "BC");

      Collection saCertificates = certStore.getCertificates(null);
      if (saCertificates == null) {
        throw new Exception("Il certificato di TSA non risulta presente");
      }
      Certificate certificate = (Certificate)saCertificates.iterator().next();
      if (certificate == null)
        throw new Exception("Il certificato di TSA non risulta presente");
      PublicKey publicKey = certificate.getPublicKey();
      if (publicKey == null) {
        throw new Exception("La publicKey della TSA non risulta presente");
      }
      Collection signers = cms.getSignerInfos().getSigners();
      SignerInformation signerInfo = (SignerInformation)signers.iterator().next();
      digestAlgorithmOID = signerInfo.getDigestAlgOID();
      MessageDigest contentDigestAlgorithm = MessageDigest.getInstance(digestAlgorithmOID);

      byte[] encodedDataToVerify = (byte[])null;
      byte[] encodedSignedData = (byte[])null;

      boolean certificateVerified = false;

      if (signerInfo.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(publicKey))) {
        certificateVerified = true;
      }
      CMSProcessable signedContent = cms.getSignedContent();
      byte[] originalContent = (byte[])signedContent.getContent();

      this.log.debug("originalContent.length: " + originalContent.length + " originalContent: " + SignerUtil.asHex(originalContent));

      byte[] encodedSignedAttributes = signerInfo.getEncodedSignedAttributes();
      if (encodedSignedAttributes != null)
        encodedDataToVerify = contentDigestAlgorithm.digest(encodedSignedAttributes);
      else {
        encodedDataToVerify = contentDigestAlgorithm.digest((byte[])cms.getSignedContent().getContent());
      }

      this.log.debug("encodedDataToVerify: " + SignerUtil.asHex(encodedDataToVerify));

      byte[] contentDigest = signerInfo.getContentDigest();

      byte[] tstInfoEncoded = contentDigestAlgorithm.digest((byte[])cms.getSignedContent().getContent());
      boolean contentVerified = Arrays.constantTimeAreEqual(contentDigest, tstInfoEncoded);

      digestAlgorithmOID = signerInfo.getEncryptionAlgOID();
      byte[] signature = signerInfo.getSignature();
      Cipher cipher = null;
      try {
        String algorithmName = null;
        if (PKCSObjectIdentifiers.rsaEncryption.getId().equals(digestAlgorithmOID))
          algorithmName = "RSA/ECB/PKCS1Padding";
        else if (PKCSObjectIdentifiers.sha1WithRSAEncryption.getId().equals(digestAlgorithmOID))
          algorithmName = "RSA/ECB/PKCS1Padding";
        else
          algorithmName = digestAlgorithmOID;
        cipher = Cipher.getInstance(algorithmName);
      }
      catch (Throwable localThrowable) {
      }
      if (cipher != null)
        try {
          this.log.debug("Cipher: " + cipher.getAlgorithm());
          cipher.init(2, publicKey);
          byte[] decryptedSignature = cipher.doFinal(signature);

          ASN1InputStream asn1is = new ASN1InputStream(decryptedSignature);
          ASN1Sequence asn1Seq = (ASN1Sequence)asn1is.readObject();

          Enumeration objs = asn1Seq.getObjects();
          while (objs.hasMoreElements()) {
            ASN1Primitive derObject = (ASN1Primitive)objs.nextElement();
            if ((derObject instanceof ASN1OctetString)) {
              ASN1OctetString octectString = (ASN1OctetString)derObject;
              encodedSignedData = octectString.getOctets();
              break;
            }
          }
          this.log.debug("encodedSignedData: " + SignerUtil.asHex(encodedSignedData));
          boolean signatureVerified = Arrays.constantTimeAreEqual(encodedSignedData, encodedDataToVerify);

          this.log.debug("Verifica timestampToken: certificateVerified = " + certificateVerified + ", signatureVerified=" + signatureVerified + ", contentVerified=" + contentVerified);
          if (!certificateVerified)
            validationInfos.addError("Il certificato non è valido");
          if (!signatureVerified)
            validationInfos.addError("La firma non è valida: l'hash di contenuto + attributi è " + SignerUtil.asHex(encodedDataToVerify) + ", mentre la firma è stata apposta su contenuto + attributi con hash: " + SignerUtil.asHex(encodedSignedData));
          if (!contentVerified)
            validationInfos.addWarning("Il contenuto non corrisponde a quanto firmato: previsto " + SignerUtil.asHex(tstInfoEncoded) + ", trovato " + SignerUtil.asHex(contentDigest));
        }
        catch (Exception e) {
          validationInfos.addWarning("Errore durante la verifica del timestamp: " + e.getMessage());
        }
    }
    catch (IOException e) {
      validationInfos.addError("Il token non contiene una marca temporale valida");
    } catch (NoSuchAlgorithmException e) {
      validationInfos.addError("Impossibile validare la marca poichè l'algoritmo di hashing non è supportato: " + digestAlgorithmOID);
    } catch (Exception e) {
      validationInfos.addError("Errore durante la validazione della marca temporale: " + e.getMessage());
    }
  }

  public InputStream getContentAsInputStream()
    throws IOException
  {
    File detachedFile = getDetachedFile();
    if (detachedFile != null) {
      return new FileInputStream(detachedFile);
    }

    InputStream contentIS = getUnsignedContent();
    return contentIS;
  }

  public String getEnclosedEnvelopeExtension()
  {
    if (this.file == null)
      return null;
    String fileName = getFile().getName();
    String extension = null;
    StringTokenizer tokenizer = new StringTokenizer(fileName, ".");
    if (tokenizer.countTokens() > 2) {
      tokenizer.nextToken();
      extension = "." + tokenizer.nextToken();
    }
    return extension;
  }

  public void setDetachedFile(File detachedFile)
  {
    if (this.detachedFiles == null)
      this.detachedFiles = new ArrayList();
    this.detachedFiles.add(0, detachedFile);
  }

  public File getDetachedFile()
  {
    return this.detachedFiles == null ? null : (File)this.detachedFiles.get(0);
  }
}