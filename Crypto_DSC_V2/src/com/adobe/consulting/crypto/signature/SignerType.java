package com.adobe.consulting.crypto.signature;

public enum SignerType
{
  CAdES_BES, CAdES_T, CAdES_C, CAdES_X_Long, XAdES, XAdES_BES, XAdES_T, XAdES_C, XAdES_X, XAdES_XL, PAdES, TSR, P7M, M7M, TSD;
}