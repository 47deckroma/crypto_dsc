package com.adobe.consulting.crypto.signature;

import com.adobe.consulting.crypto.bean.SignerBean;
import com.adobe.consulting.crypto.bean.ValidationInfos;
import java.util.List;

public abstract interface ISignature
{
  public abstract byte[] getSignatureBytes();

  public abstract SignerBean getSignerBean();

  public abstract ValidationInfos verify();

  public abstract List<ISignature> getCounterSignatures();
}