package com.adobe.consulting.crypto.signature;

import com.adobe.consulting.crypto.bean.SignerBean;
import com.adobe.consulting.crypto.bean.ValidationInfos;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.List;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.jcajce.JcaSimpleSignerInfoVerifierBuilder;
import org.bouncycastle.util.Arrays;

public class CMSSignature
  implements ISignature
{
  private SignerInformation signerInformation;
  private List<ISignature> counterSignatures;
  private SignerBean signerBean;
  private List<File> detachedFiles;

  public CMSSignature(SignerInformation signerInformation, X509Certificate certificate)
  {
    this.detachedFiles = null;
    this.signerInformation = signerInformation;
    SignerBean signerBean = new SignerBean();
    signerBean.setCertificate(certificate);
    signerBean.setIusser(certificate.getIssuerX500Principal());
    signerBean.setSubject(certificate.getSubjectX500Principal());
    this.signerBean = signerBean;
  }

  public CMSSignature(SignerInformation signerInformation, X509Certificate certificate, List<File> detachedFiles)
  {
    this(signerInformation, certificate);
    this.detachedFiles = detachedFiles;
  }

  public byte[] getSignatureBytes() {
    return this.signerInformation.getSignature();
  }

  public SignerBean getSignerBean() {
    return this.signerBean;
  }

  public ValidationInfos verify() {
    ValidationInfos validationInfos = new ValidationInfos();

    if (this.detachedFiles == null) {
      try {
        this.signerInformation.verify(new JcaSimpleSignerInfoVerifierBuilder().setProvider("BC").build(this.signerBean.getCertificate()));
      } catch (CMSException e) {
        validationInfos.addError("Il certificato di firma non è valido");
      } catch (Exception e) {
        validationInfos.addError(e.getMessage());
      }

    }
    else
    {
      File detachedFile = (File)this.detachedFiles.get(0);
      FileInputStream fis = null;
      try {
        fis = new FileInputStream(detachedFile);
        if (validationInfos != null)
          validationInfos = verifyDetachedContent(this.signerInformation, fis);
      }
      catch (FileNotFoundException e) {
        e.printStackTrace();

        if (fis != null)
          try {
            fis.close();
          }
          catch (IOException e2) {
            e2.printStackTrace();
          }
      }
      finally
      {
        if (fis != null) {
          try {
            fis.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }

        }

      }

    }

    return validationInfos;
  }

  public SignerInformation getSignerInformation() {
    return this.signerInformation;
  }

  public void setSignerInformation(SignerInformation signerInformation) {
    this.signerInformation = signerInformation;
    SignerId signerId = signerInformation.getSID();
  }

  public String toString()
  {
    return "Signature: " + this.signerInformation == null ? "" : getSignerBean().toString();
  }

  public void setCounterSignatures(List<ISignature> counterSignatures) {
    this.counterSignatures = counterSignatures;
  }

  public List<ISignature> getCounterSignatures() {
    return this.counterSignatures;
  }

  public List<File> getDetachedFiles() {
    return this.detachedFiles;
  }

  public void setDetachedFiles(List<File> detachedFiles) {
    this.detachedFiles = detachedFiles;
  }

  protected ValidationInfos verifyDetachedContent(SignerInformation signerInformation, InputStream detachedContent)
  {
    ValidationInfos validationInfos = new ValidationInfos();

    String digestAlgorithmOID = signerInformation.getDigestAlgOID();
    try
    {
      MessageDigest contentDigestAlgorithm = MessageDigest.getInstance(digestAlgorithmOID);

      byte[] hashedDetachedData = (byte[])null;

      byte[] hashedSignedAttributes = (byte[])null;

      byte[] decodedSignature = (byte[])null;

      byte[] digestSignedAttribute = (byte[])null;

      byte[] buff = new byte[4096];
      int length = -1;
      contentDigestAlgorithm.reset();
      while ((length = detachedContent.read(buff)) != -1) {
        contentDigestAlgorithm.update(buff, 0, length);
      }
      hashedDetachedData = contentDigestAlgorithm.digest();

      AttributeTable signedAttributeTable = signerInformation.getSignedAttributes();
      hashedSignedAttributes = contentDigestAlgorithm.digest(signerInformation.getEncodedSignedAttributes());

      digestAlgorithmOID = signerInformation.getEncryptionAlgOID();
      byte[] signature = signerInformation.getSignature();
      Cipher cipher = null;
      String algorithmName = null;
      if (PKCSObjectIdentifiers.rsaEncryption.getId().equals(digestAlgorithmOID))
        algorithmName = "RSA/ECB/PKCS1Padding";
      else if (PKCSObjectIdentifiers.sha1WithRSAEncryption.getId().equals(digestAlgorithmOID))
        algorithmName = "RSA/ECB/PKCS1Padding";
      else
        algorithmName = digestAlgorithmOID;
      cipher = Cipher.getInstance(algorithmName);
      cipher.init(2, this.signerBean.getCertificate().getPublicKey());
      byte[] decryptedSignature = cipher.doFinal(signature);

      ASN1InputStream asn1is = new ASN1InputStream(decryptedSignature);
      ASN1Sequence asn1Seq = (ASN1Sequence)asn1is.readObject();

      Enumeration objs = asn1Seq.getObjects();
      while (objs.hasMoreElements()) {
        ASN1Primitive derObject = (ASN1Primitive)objs.nextElement();
        if ((derObject instanceof ASN1OctetString)) {
          ASN1OctetString octectString = (ASN1OctetString)derObject;
          decodedSignature = octectString.getOctets();
          break;
        }
      }

      boolean signatureVerified = Arrays.constantTimeAreEqual(decodedSignature, hashedSignedAttributes);
      if (!signatureVerified) {
        validationInfos.addError("La firma non e valida: l'hash degli attributi firmati e " + hashedSignedAttributes + " mentre la firma e stata apposta su un contenuto con hash: " + decodedSignature);
      }
      else {
        Attribute digestAttribute = signedAttributeTable.get(PKCSObjectIdentifiers.pkcs_9_at_messageDigest);
        ASN1Set values = digestAttribute.getAttrValues();
        ASN1Primitive derObject = values.getObjectAt(0).toASN1Primitive();
        if ((derObject instanceof ASN1OctetString)) {
          ASN1OctetString octectString = (ASN1OctetString)derObject;
          digestSignedAttribute = octectString.getOctets();
        }

        boolean contentDigestVerified = Arrays.constantTimeAreEqual(hashedDetachedData, digestSignedAttribute);
        if (!contentDigestVerified)
          validationInfos.addError("La firma e valida ma non e associata al file corretto");
      }
    } catch (NoSuchAlgorithmException e) {
      validationInfos.addError("Impossibile validare la firma poiche l'algoritmo non e supportato: " + digestAlgorithmOID);
    } catch (IOException e) {
      validationInfos.addError("Errore durante la validazione della firma: " + e.getMessage());
    } catch (GeneralSecurityException e) {
      validationInfos.addError("Impossibile decifrare la firma: " + e.getMessage());
    }

    return validationInfos;
  }
}