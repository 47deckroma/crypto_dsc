package com.adobe.consulting.crypto.utils;

import com.adobe.idp.Context;
import com.adobe.idp.dsc.clientsdk.ServiceClientFactory;
import com.adobe.idp.dsc.registry.ConfigParameterNotFoundException;
import com.adobe.idp.dsc.registry.RegistryException;
import com.adobe.idp.dsc.registry.infomodel.ServiceConfiguration;
import com.adobe.idp.dsc.registry.service.client.ServiceRegistryClient;
import com.adobe.idp.um.api.UMException;
import com.adobe.idp.um.api.UMLocalUtils;
//import com.adobe.idp.um.api.UMUtil;


public class ServiceConfigurationHandler
{
  public static ServiceConfiguration getActiveConfiguration(String serviceName)
  {
    try
    {//Aggiungere um.jar nel classpath
      Context _sysCnxt = UMLocalUtils.getSystemContext();

      ServiceClientFactory _svcClientFactory = ServiceClientFactory.createInstance(_sysCnxt);

      ServiceRegistryClient _svcRegClient = new ServiceRegistryClient(_svcClientFactory);

      return _svcRegClient.getHeadActiveConfiguration(serviceName);
    }
    catch (RegistryException e)
    {
      e.printStackTrace();
    }
    catch (UMException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static String getConfigurationProperty(String serviceName, String configurationParameterName)
  {
    try
    {
      ServiceConfiguration _serviceConfiguration = null;

      _serviceConfiguration = getActiveConfiguration(serviceName);

      return _serviceConfiguration.getConfigParameter(configurationParameterName).getTextValue();
    }
    catch (ConfigParameterNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }
}