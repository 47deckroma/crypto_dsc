package com.adobe.consulting.crypto.bean;

import java.util.StringTokenizer;

public class ValidationInfos
{
  String[] warnings = null;

  String[] errors = null;
  private static final String WARNINGS_CODE = "warnings";
  private static final String ERRORS_CODE = "errors";
  private static final String ISVALID_CODE = "isValid";

  public boolean isValid()
  {
    return (this.errors == null) || (this.errors.length == 0);
  }

  public boolean isValid(boolean strict)
  {
    return strict ? false : (isValid()) && ((this.warnings == null) || (this.warnings.length == 0)) ? true : isValid();
  }

  public void addWarning(String message)
  {
    int length = this.warnings == null ? 0 : this.warnings.length;
    String[] temp = new String[length + 1];
    if (this.warnings != null)
      System.arraycopy(this.warnings, 0, temp, 0, length);
    temp[length] = message;
    this.warnings = temp;
  }

  public void addWarnings(String[] newWarnings)
  {
    if ((newWarnings == null) || (newWarnings.length == 0))
      return;
    int length = this.warnings == null ? 0 : this.warnings.length;
    String[] temp = new String[length + newWarnings.length];
    if (this.warnings != null)
      System.arraycopy(this.warnings, 0, temp, 0, length);
    for (int i = 0; i < newWarnings.length; i++)
      temp[(length + i)] = newWarnings[i];
    this.warnings = temp;
  }

  public void addError(String error)
  {
    int length = this.errors == null ? 0 : this.errors.length;
    String[] temp = new String[length + 1];
    if (this.errors != null)
      System.arraycopy(this.errors, 0, temp, 0, length);
    temp[length] = error;
    this.errors = temp;
  }

  public void addErrors(String[] newErrors)
  {
    if ((newErrors == null) || (newErrors.length == 0))
      return;
    int length = this.errors == null ? 0 : this.errors.length;
    String[] temp = new String[length + newErrors.length];
    if (this.errors != null)
      System.arraycopy(this.errors, 0, temp, 0, length);
    for (int i = 0; i < newErrors.length; i++)
      temp[(length + i)] = newErrors[i];
    this.errors = temp;
  }

  public String[] getWarnings()
  {
    return this.warnings;
  }

  public String[] getErrors()
  {
    return this.errors;
  }

  public String getErrorsString() {
    StringBuffer errorsBuf = new StringBuffer();
    if (this.errors != null) {
      for (int i = 0; i < this.errors.length; i++) {
        errorsBuf.append(this.errors[i]);
        if (i != this.errors.length - 1)
          errorsBuf.append(", ");
      }
    }
    return errorsBuf.toString();
  }

  public String getWarningsString() {
    StringBuffer warningsBuf = new StringBuffer();
    if (this.warnings != null) {
      for (int i = 0; i < this.warnings.length; i++) {
        warningsBuf.append(this.warnings[i]);
        if (i != this.warnings.length - 1)
          warningsBuf.append(", ");
      }
    }
    return warningsBuf.toString();
  }

  public String toString()
  {
    StringBuffer warningsBuf = new StringBuffer("[");
    if (this.warnings != null)
      for (int i = 0; i < this.warnings.length; i++)
        if (i != this.warnings.length - 1)
          warningsBuf.append(this.warnings[i] + ", ");
        else
          warningsBuf.append(this.warnings[i] + "]");
    else
      warningsBuf.append("]");
    StringBuffer errorsBuf = new StringBuffer("[");
    if (this.errors != null) {
      for (int i = 0; i < this.errors.length; i++)
        if (i != this.errors.length - 1)
          errorsBuf.append(this.errors[i] + ", ");
        else
          errorsBuf.append(this.errors[i] + "]");
    }
    else
      errorsBuf.append("]");
    return "warnings: " + warningsBuf + ", " + "errors" + ": " + errorsBuf + ", " + "isValid" + ": " + isValid();
  }

  public static ValidationInfos fromString(String string) {
    if (string == null)
      return null;
    ValidationInfos result = new ValidationInfos();

    String warnings = null;
    int wStartIdx = string.indexOf("warnings");
    if (wStartIdx != -1) {
      warnings = string.substring(wStartIdx);
      wStartIdx = warnings.indexOf("[");
      warnings = warnings.substring(wStartIdx);
      int nOpen = 0;
      for (int i = 0; i < warnings.length(); i++) {
        if (warnings.charAt(i) == '[') nOpen++;
        else if (warnings.charAt(i) == ']') nOpen--;
        if (nOpen == 0) {
          warnings = warnings.substring(1, i);
          break;
        }
      }
    }

    String errors = null;
    int eStartIdx = string.indexOf("errors");
    if (eStartIdx != -1) {
      errors = string.substring(eStartIdx);
      eStartIdx = errors.indexOf("[");
      errors = errors.substring(eStartIdx);
      int nOpen = 0;
      for (int i = 0; i < errors.length(); i++) {
        if (errors.charAt(i) == '[') nOpen++;
        else if (errors.charAt(i) == ']') nOpen--;
        if (nOpen == 0) {
          errors = errors.substring(1, i);
          break;
        }
      }
    }

    StringTokenizer wTokenizer = new StringTokenizer(warnings, ",");
    while (wTokenizer.hasMoreElements()) {
      String wToken = wTokenizer.nextToken();
      result.addWarning(wToken);
    }

    StringTokenizer eTokenizer = new StringTokenizer(errors, ",");
    while (eTokenizer.hasMoreElements()) {
      String eToken = eTokenizer.nextToken();
      result.addError(eToken);
    }

    return result;
  }

  public void setWarnings(String[] warnings) {
    this.warnings = warnings;
  }

  public void setErrors(String[] errors) {
    this.errors = errors;
  }
}