package com.adobe.consulting.crypto;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSSignedDataParser;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Arrays;

import com.adobe.consulting.crypto.bean.ValidationInfos;
import com.adobe.consulting.crypto.signature.SignerType;

public class CAdESSigner extends P7MSigner
{
  Logger Log = Logger.getLogger(CAdESSigner.class);

  private SignerType type = null;

  private Map<byte[], TimeStampToken> timestamptokensBySignature = null;

  public TimeStampToken[] getTimeStampTokens()
  {
    InputStream stream = null;
    ArrayList timestampTokensList = new ArrayList();
    if (this.timestamptokens == null) {
      try {
        CMSSignedDataParser cmsSignedData = null;
        if (isBase64()) {
          Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
          cmsSignedData = new CMSSignedDataParser(streambase64);
        } else {
          cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
        }
        cmsSignedData.getSignedContent().drain();
        SignerInformationStore signersStore = cmsSignedData.getSignerInfos();
        Collection<SignerInformation> signers = signersStore.getSigners();

        this.timestamptokensBySignature = new HashMap();
        if (signers != null) {
          for (SignerInformation signer : signers) {
            AttributeTable table = signer.getUnsignedAttributes();
            if (table == null)
              return null;
            
            Attribute attribute = (Attribute)table.toHashtable().get(new DERObjectIdentifier(PKCSObjectIdentifiers.id_aa_signingCertificateV2.getId()));
            if ((attribute != null) && (attribute.getAttrValues() != null)) {
              TimeStampToken timestamptoken = null;
              try {
                timestamptoken = new TimeStampToken(new CMSSignedData(attribute.getAttrValues().getObjectAt(0).toASN1Primitive().getEncoded()));
              } catch (Exception e) {
                e.printStackTrace();
              }
              if (timestamptoken != null) {
                timestampTokensList.add(timestamptoken);
                this.timestamptokensBySignature.put(signer.getSignature(), timestamptoken);
              }
            }
          }
        }
        if (timestampTokensList.size() != 0)
          this.timestamptokens = ((TimeStampToken[])timestampTokensList.toArray(new TimeStampToken[timestampTokensList.size()]));
      } catch (Exception e) {
        e.printStackTrace();
        this.timestamptokens = null;
      } finally {
        if (stream != null) {
          IOUtils.closeQuietly(stream);
        }
      }
    }

    return this.timestamptokens;
  }

  protected boolean isSignedType(CMSSignedDataParser cmsSignedDataInternal)
  {
    reset();
    this.type = null;
    this.timestamptokensBySignature = null;

    boolean signed = false;
    try {
      cmsSignedDataInternal.getSignedContent().drain();

      SignerInformationStore signersStore = cmsSignedDataInternal.getSignerInfos();
      Collection<SignerInformation> signers = signersStore.getSigners();

      if ((signers == null) || (signers.isEmpty())) {
        signed = false;
      }
      else {
        for (SignerInformation signer : signers) {
          AttributeTable signedTable = signer.getSignedAttributes();
          boolean certv2 = signedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signingCertificateV2);
          boolean cert = signedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signingCertificate);
          if ((certv2) && (cert)) {
            signed = false;
            break;
          }

          if ((!CMSSignedDataGenerator.DIGEST_SHA256.equals(signer.getDigestAlgOID())) || ((!certv2) && (cert))) {
            this.Log.error("Il file non si attiene alla specifica CAdES_BES");
            signed = false;
            break;
          }

          signed = true;

          if (this.type == null) {
            this.type = SignerType.CAdES_BES;
          }
          this.Log.info("Il file si attiene alla specifica CAdES_BES");

          AttributeTable unsignedTable = signer.getUnsignedAttributes();
          if ((unsignedTable != null) && (unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_signatureTimeStampToken))) {
            this.type = SignerType.CAdES_T;

            this.Log.info("Il file si attiene alla specifica CAdES_T");

            if ((unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_ets_certificateRefs)) && (unsignedTable.toHashtable().containsKey(PKCSObjectIdentifiers.id_aa_ets_revocationRefs))) {
              this.type = SignerType.CAdES_C;

              this.Log.info("Il file si attiene alla specifica CAdES_C");
            }
            else {
              this.Log.error("Il file non si attiene alla specifica CAdES_C");
            }
          } else {
            this.Log.error("Il file non si attiene alla specifica CAdES_T");
          }
        }
      }

    }
    catch (Exception e)
    {
      signed = false;
    }
    return signed;
  }

  public boolean isSignedType(File file)
  {
    boolean signed = false;
    InputStream stream = null;
    this.file = file;
    try {
      stream = FileUtils.openInputStream(file);

      String CMSType = getCMSType(stream);

      stream.close();

      if (CMSType.equals(PKCSObjectIdentifiers.signedData.getId())) {
        this.Log.info("CMSType is SignedData");
      }

      if (CMSType.equals(PKCSObjectIdentifiers.encryptedData.getId())) {
        this.Log.info("CMSType is EncryptedData");
      }

      if (CMSType.equals(PKCSObjectIdentifiers.envelopedData.getId())) {
        this.Log.info("CMSType is EnvelopedData");
      }

      stream = FileUtils.openInputStream(file);

      CMSSignedDataParser parser = new CMSSignedDataParser(stream);
      signed = isSignedType(parser);
      stream.close();
    }
    catch (Exception e) {
      try {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(file));
        CMSSignedDataParser parser = new CMSSignedDataParser(streambase64);
        signed = isSignedType(parser);
        setBase64(true);
      } catch (Exception er) {
        signed = false;
        try {
          if (stream != null)
            stream.close();
        }
        catch (IOException localIOException) {
        }
      }
    }
    return signed;
  }

  private String getCMSType(InputStream stream) {
    ASN1InputStream is = new ASN1InputStream(stream);
    try
    {
      Enumeration enu = ((ASN1Sequence)is.readObject()).getObjects();

      DERObjectIdentifier doi = (DERObjectIdentifier)enu.nextElement();
      return doi.getId();
    }
    catch (ClassCastException localClassCastException)
    {
      return null;
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      try {
        is.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }

    return null;
  }

  public SignerType getFormat()
  {
    return this.type;
  }

  public ValidationInfos validateTimeStampTokensEmbedded()
    throws CMSException
  {
    ValidationInfos validationInfos = new ValidationInfos();

    if (this.type == SignerType.CAdES_BES) {
      validationInfos.addError("Il formato: " + this.type + " non contiene una marca temporale");
      return validationInfos;
    }

    if (this.timestamptokens == null) {
      if (!isSignedType(this.file)) {
        validationInfos.addError("File non in formato: " + getFormat());
        return validationInfos;
      }
      getTimeStampTokens();
    }
    CMSSignedDataParser cmsSignedData = null;
    try {
      if (isBase64()) {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
        cmsSignedData = new CMSSignedDataParser(streambase64);
      } else {
        cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
      }
    } catch (IOException e1) {
      throw new CMSException("Generic", e1);
    }

    SignerInformationStore signersStore = cmsSignedData.getSignerInfos();
    Collection<SignerInformation> signers = signersStore.getSigners();
    if (signers != null) {
      for (SignerInformation signerInfo : signers)
      {
        byte[] signature = signerInfo.getSignature();
        Set<byte[]> signatures = this.timestamptokensBySignature.keySet();
        TimeStampToken timestamptoken = null;
        for (byte[] byteSignature : signatures) {
          if (Arrays.areEqual(byteSignature, signature)) {
            timestamptoken = (TimeStampToken)this.timestamptokensBySignature.get(byteSignature);
            break;
          }
        }
        String hashAlgOID = timestamptoken.getTimeStampInfo().getMessageImprintAlgOID().getId();
        try
        {
          MessageDigest digest = MessageDigest.getInstance(hashAlgOID);
          TimeStampRequestGenerator gen = new TimeStampRequestGenerator();
          TimeStampRequest request = gen.generate(hashAlgOID, digest.digest(signature));

          checkTimeStampTokenOverRequest(validationInfos, timestamptoken, request);
        }
        catch (NoSuchAlgorithmException e) {
          validationInfos.addError("Impossibile validare la marca poichè l'algoritmo di calcolo non è supportato: " + hashAlgOID);
        }
      }
    }

    return validationInfos;
  }
}