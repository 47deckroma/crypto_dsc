package com.adobe.consulting.crypto;

import com.adobe.consulting.crypto.signature.AbstractSigner;
import com.adobe.consulting.crypto.signature.CMSSignature;
import com.adobe.consulting.crypto.signature.ISignature;
import com.adobe.consulting.crypto.signature.SignerType;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.cert.CRL;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.security.auth.x500.X500Principal;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.ess.ESSCertID;
import org.bouncycastle.asn1.ess.ESSCertIDv2;
import org.bouncycastle.asn1.ess.SigningCertificate;
import org.bouncycastle.asn1.ess.SigningCertificateV2;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSSignedDataParser;
import org.bouncycastle.cms.CMSTypedStream;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.Store;
import org.bouncycastle.x509.X509Store;

public class P7MSigner extends AbstractSigner
{
  protected boolean isSignedType(CMSSignedDataParser signedData)
    throws CMSException
  {
    reset();

    boolean signed = false;
    try {
      signedData.getSignedContent().drain();
    } catch (Exception e) {
      throw new CMSException("Errore firma", e);
    }
    SignerInformationStore signersStore = signedData.getSignerInfos();
    signersStore = signedData.getSignerInfos();

    Collection<SignerInformation> signers = signersStore.getSigners();
    if ((signers == null) || (signers.isEmpty())) {
      signed = false;
    }
    else {
      for (SignerInformation signer : signers) {
        if (!CMSSignedDataGenerator.DIGEST_SHA1.equals(signer.getDigestAlgOID())) {
          signed = false;
          break;
        }
        signed = true;
      }
    }

    return signed;
  }

  public boolean isSignedType(File file)
  {
    boolean signed = false;
    InputStream stream = null;
    this.file = file;
    try {
      stream = FileUtils.openInputStream(file);
      CMSSignedDataParser cmsSignedData = new CMSSignedDataParser(stream);
      signed = isSignedType(cmsSignedData);
      stream.close();
    } catch (Exception e) {
      try {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(file));
        CMSSignedDataParser parser = new CMSSignedDataParser(streambase64);
        signed = isSignedType(parser);
        setBase64(true);
      }
      catch (Exception er) {
        signed = false;
        try {
          if (stream != null)
            stream.close();
        } catch (IOException localIOException) {
        }
      }
    } finally {
      if (stream != null) {
        IOUtils.closeQuietly(stream);
      }
    }
    return signed;
  }

  public TimeStampToken[] getTimeStampTokens()
  {
    return null;
  }

  public SignerType getFormat()
  {
    return SignerType.P7M;
  }

  public static InputStream getCMSSignedDataUnsignedContent(CMSSignedDataParser sd)
  {
    return sd.getSignedContent().getContentStream();
  }

  protected InputStream getExtractedContent() throws IOException, CMSException {
    CMSSignedDataParser cmsSignedData = null;
    if (isBase64()) {
      Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
      cmsSignedData = new CMSSignedDataParser(streambase64);
    } else {
      cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
    }
    return cmsSignedData.getSignedContent().getContentStream();
  }

  public InputStream getUnsignedContent() {
    try {
      File detachedFile = getDetachedFile();

      if (detachedFile != null) {
        return FileUtils.openInputStream(detachedFile);
      }

      return getExtractedContent();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return null;
  }

  public byte[] getUnsignedContentDigest(MessageDigest digestAlgorithm)
  {
    InputStream unsignedContent = getUnsignedContent();
    byte[] buff = new byte[4096];
    int length = -1;
    digestAlgorithm.reset();
    try {
      while ((length = unsignedContent.read(buff)) != -1) {
        digestAlgorithm.update(buff, 0, length);
      }
      return digestAlgorithm.digest();
    }
    catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (unsignedContent != null)
        try {
          unsignedContent.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
    }
    return null;
  }

  public static final List<ISignature> getISigneturesFromCMSSignedData(CMSSignedDataParser signedData, List<File> detachedContent)
    throws CMSException
  {
    List result = new ArrayList();

    Collection certificates = null;
    try {
      Store storecertificate = signedData.getCertificates();
      certificates = storecertificate.getMatches(null);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    Collection<SignerInformation> signers = signedData.getSignerInfos().getSigners();
    for (SignerInformation signer : signers) {
      SignerId signerID = signer.getSID();
      ISignature signature = getISignatureFromSignerInformationAndCertificates(signer, certificates, detachedContent);
      if (signature != null)
        result.add(signature);
    }
    return result;
  }

  protected static final List<ISignature> getISigneturesFromCMSSignedDataInternal(CMSSignedData signedData, List<File> detachedContent) throws CMSException
  {
    List result = new ArrayList();

    Collection certificates = null;
    try {
      Store storecertificate = signedData.getCertificates();
      certificates = storecertificate.getMatches(null);
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    Collection<SignerInformation> signers = signedData.getSignerInfos().getSigners();
    for (SignerInformation signer : signers) {
      SignerId signerID = signer.getSID();
      ISignature signature = getISignatureFromSignerInformationAndCertificates(signer, certificates, detachedContent);
      if (signature != null)
        result.add(signature);
    }
    return result;
  }

  public static ISignature getISignatureFromSignerInformationAndCertificates(SignerInformation signer, Collection<Certificate> certificates, List<File> detachedContent)
  {
    SignerId signerID = signer.getSID();

    AttributeTable signedTable = signer.getSignedAttributes();
    Attribute signingCertificateV2Attr = null;
    Attribute signingCertificateAttr = null;
    if (signedTable != null) {
      signingCertificateV2Attr = (Attribute)signedTable.toHashtable().get(PKCSObjectIdentifiers.id_aa_signingCertificateV2);
      signingCertificateAttr = (Attribute)signedTable.toHashtable().get(PKCSObjectIdentifiers.id_aa_signingCertificate);
    }

    byte[] certHash = (byte[])null;
    String certHashAlgorithmOid = null;
    try
    {
      if (signingCertificateV2Attr != null)
      {
        try
        {
          SigningCertificateV2 signingCertificateV2 = SigningCertificateV2.getInstance(signingCertificateV2Attr.getAttrValues().getObjectAt(0));
          ESSCertIDv2[] essCertsV2 = signingCertificateV2.getCerts();
          ESSCertIDv2 essCertV2 = essCertsV2[0];
          certHash = essCertV2.getCertHash();
          certHashAlgorithmOid = essCertV2.getHashAlgorithm().getObjectId().getId();
        }
        catch (Exception e)
        {
          ASN1Sequence signingCertificateV2Encoded = (ASN1Sequence)signingCertificateV2Attr.getAttrValues().getObjectAt(0);
          ASN1Sequence signingCertificateV2Certs = ASN1Sequence.getInstance(signingCertificateV2Encoded.getObjectAt(0));
          certHash = ASN1OctetString.getInstance(signingCertificateV2Certs.getObjectAt(0).toASN1Primitive()).getOctets();

          certHashAlgorithmOid = CMSSignedDataGenerator.DIGEST_SHA256;
        }
      }
      else if (signingCertificateAttr != null) {
        try
        {
          SigningCertificate signingCertificate = SigningCertificate.getInstance(signingCertificateAttr.getAttrValues().getObjectAt(0));
          if (signingCertificateAttr != null) {
            ESSCertID[] essCertsV2 = signingCertificate.getCerts();
            ESSCertID essCert = essCertsV2[0];
            certHash = essCert.getCertHash();
            certHashAlgorithmOid = CMSSignedDataGenerator.DIGEST_SHA1;
          }
        }
        catch (Exception e)
        {
          ASN1Sequence signingCertificateEncoded = (ASN1Sequence)signingCertificateAttr.getAttrValues().getObjectAt(0);
          ASN1Sequence signingCertificateCerts = ASN1Sequence.getInstance(signingCertificateEncoded.getObjectAt(0));
          certHash = ASN1OctetString.getInstance(signingCertificateCerts.getObjectAt(0).toASN1Primitive()).getOctets();
          certHashAlgorithmOid = CMSSignedDataGenerator.DIGEST_SHA1;
        }

      }

    }
    catch (Exception localException1)
    {
    }
    
    for (Iterator iterator = certificates.iterator(); iterator.hasNext();) {
    
    	Object obj = ((Iterator) certificates).next();

      Certificate certificate = null;

      if ((obj instanceof X509CertificateHolder))
        try {
          certificate = new JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME).getCertificate((X509CertificateHolder)obj);
        } catch (CertificateException e) {
          e.printStackTrace();
        }
      else {
        certificate = (Certificate)obj;
      }

      boolean correctCertificate = false;

      if (certHash != null) {
        try {
          MessageDigest digest = MessageDigest.getInstance(certHashAlgorithmOid);
          if (digest == null)
            return null;
          byte[] computedCertificateHash = digest.digest(certificate.getEncoded());
          if (!Arrays.areEqual(certHash, computedCertificateHash)) break;
          correctCertificate = true;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      else if (((certificate instanceof X509Certificate)) && 
        (((X509Certificate)certificate).getIssuerX500Principal().equals(signerID.getIssuer())) && 
        (signerID.getSerialNumber().equals(((X509Certificate)certificate).getSerialNumber())))
      {
        correctCertificate = true;
      }

      label466: if (correctCertificate) {
        CMSSignature cmsSignature = new CMSSignature(signer, (X509Certificate)certificate);
        cmsSignature.setDetachedFiles(detachedContent);

        SignerInformationStore counterSignaturesStore = signer.getCounterSignatures();
        Collection<SignerInformation> counterSignaturesInfo = counterSignaturesStore.getSigners();
        if (counterSignaturesInfo != null) {
          List counterSignatures = new ArrayList();
          for (SignerInformation counterSignatureInfo : counterSignaturesInfo)
            counterSignatures.add(getISignatureFromSignerInformationAndCertificates(counterSignatureInfo, certificates, null));
          cmsSignature.setCounterSignatures(counterSignatures);
        }

        return cmsSignature;
      }
    }
    return null;
  }

  public List<ISignature> getSignatures()
  {
    try {
      CMSSignedDataParser cmsSignedData = null;
      if (isBase64()) {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
        cmsSignedData = new CMSSignedDataParser(streambase64);
      } else {
        cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
      }
      cmsSignedData.getSignedContent().drain();
      return getISigneturesFromCMSSignedData(cmsSignedData, this.detachedFiles);
    }
    catch (Exception localException) {
    }
    return null;
  }

  public boolean canContentBeSigned()
  {
    return true;
  }

  public static Collection<CRL> getCRLsFromCMSSignedData(CMSSignedDataParser cmsSignedData)
  {
    Collection crls = null;
    try {
      X509Store store = cmsSignedData.getCRLs("Collection", BouncyCastleProvider.PROVIDER_NAME);
      return store.getMatches(null);
    } catch (Exception e) {
    }
    return null;
  }

  public static Collection<? extends Certificate> getCertificatesFromCMSSignedData(CMSSignedDataParser cmsSignedData)
  {
    try
    {
      CertStore store = cmsSignedData.getCertificatesAndCRLs("Collection", BouncyCastleProvider.PROVIDER_NAME);
      return store.getCertificates(null); } catch (Exception e) {
    }
    return null;
  }

  public Collection<CRL> getEmbeddedCRLs()
  {
    try {
      CMSSignedDataParser cmsSignedData = null;
      if (isBase64()) {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
        cmsSignedData = new CMSSignedDataParser(streambase64);
      } else {
        cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
      }
      cmsSignedData.getSignedContent().drain();
      return getCRLsFromCMSSignedData(cmsSignedData);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public Collection<? extends Certificate> getEmbeddedCertificates()
  {
    try {
      CMSSignedDataParser cmsSignedData = null;
      if (isBase64()) {
        Base64InputStream streambase64 = new Base64InputStream(FileUtils.openInputStream(this.file));
        cmsSignedData = new CMSSignedDataParser(streambase64);
      } else {
        cmsSignedData = new CMSSignedDataParser(FileUtils.openInputStream(this.file));
      }
      cmsSignedData.getSignedContent().drain();
      return getCertificatesFromCMSSignedData(cmsSignedData);
    }
    catch (Exception localException) {
    }
    return null;
  }
}