package com.adobe.consulting.digest;

import com.adobe.idp.Document;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.log4j.Logger;

public class CEncoder
{
  static Logger Log = Logger.getLogger(CEncoder.class);

  public static boolean hashesEqual(Document doc, String originalHash) {
    String newHash = generateMD5Hash(doc);
    Log.info("GENERATED HASH: " + newHash);
    return newHash.equals(originalHash);
  }

  public static String generateMD5Hash(Document doc)
  {
    String ret = "";
    byte[] data = (byte[])null;
    try
    {
      data = document2byte(doc);
      ret = generateHashMD5(data);
    }
    catch (Exception e)
    {
      Log.error("generateMD5Hash err: " + e.getMessage());
    }
    finally {
      data = (byte[])null;
    }

    return ret;
  }

  private static String generateHashMD5(byte[] data)
    throws NoSuchAlgorithmException, FileNotFoundException, IOException
  {
    MessageDigest md = MessageDigest.getInstance("MD5");
    String hash = "";

    md.update(data);

    byte[] digest = md.digest();
    int[] digestI = new int[digest.length];

    for (int i = 0; i < digest.length; i++)
    {
      digestI[i] = (digest[i] >= 0 ? digest[i] : digest[i] + 256);
    }

    for (int i = 0; i < digest.length; i++)
    {
      String hex = Integer.toHexString(digestI[i]);
      if (hex.length() == 1) hex = "0" + hex;
      hex = hex.substring(hex.length() - 2);
      hash = hash + hex;
    }

    digest = (byte[])null;
    digestI = (int[])null;

    return hash.toUpperCase();
  }

  private static byte[] document2byte(Document doc) throws Exception
  {
    InputStream is = doc.getInputStream();

    byte[] theBytes = new byte[is.available()];

    is.read(theBytes);

    return (byte[])theBytes.clone();
  }
}