package com.adobe.consulting.signature;

import com.adobe.consulting.types.PDFVerifyResult;
import com.adobe.consulting.types.VerifySignatureResult;
import com.adobe.consulting.types.v2.PDFVerifyResult2;
import com.adobe.consulting.types.v2.SignatureResult;
import com.adobe.consulting.types.v2.VerifyStatus;
import com.adobe.idp.Document;
import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.security.auth.x500.X500Principal;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.PolicyInformation;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import org.bouncycastle.cert.ocsp.OCSPReq;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.DigestCalculatorProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

public class PDF
{
  Logger Log = Logger.getLogger(PDF.class);
  protected String m_castorefolder;
  protected String m_useprotocol;
  protected String m_proxyHost;
  protected String m_proxyPort;
  protected String m_proxyUser;
  protected String m_proxyPassword;
  protected boolean m_useProxy = false;
  

  public void setProxyHost(String aHost)
  {
    if (aHost != null) {
      aHost = aHost.trim();
    }
    this.m_proxyHost = aHost;
  }

  public String getProxyHost()
  {
    return this.m_proxyHost;
  }

  public void setProxyPort(String aHost) {
    if (aHost != null) {
      aHost = aHost.trim();
    }
    this.m_proxyPort = aHost;
  }

  public String getProxyPort()
  {
    return this.m_proxyPort;
  }

  public void setProxyUser(String aUser)
  {
    if (aUser != null) {
      aUser = aUser.trim();
    }
    this.m_proxyUser = aUser;
  }

  public String getProxyUser()
  {
    return this.m_proxyUser;
  }

  public void setProxyPassword(String aPassword)
  {
    if (aPassword != null) {
      aPassword = aPassword.trim();
    }
    this.m_proxyPassword = aPassword;
  }

  public String getProxyPassword()
  {
    return this.m_proxyPassword;
  }

  public boolean isPDF(Document pdfDoc)
  {
    PdfReader reader = null;

    InputStream is = null;
    try
    {
      is =pdfDoc.getInputStream();
      reader = new PdfReader(is);
      reader.close();
      this.Log.info("File is a real PDF");
      is.close();
      return true;
    }
    catch (InvalidPdfException localInvalidPdfException) {
    }
    catch (IOException localIOException) {
    }
    catch (Exception localException) {
    }
    finally {
      if (reader != null) reader.close();
      if(is !=null)
		try {
			is.close();
		} catch (IOException e) {}
    }

    return false;
  }

  public PDFVerifyResult2 verifyPDF2(Document pdfDoc, String caStoreFolder, String caStoreName, ProtocolTypes useProtocol,boolean updateTrustAnchorsStore) throws VerifyException, TSLUpdateException, org.bouncycastle.cert.ocsp.OCSPException {
    return verifyPDF2(pdfDoc.getInputStream(), caStoreFolder, caStoreName, useProtocol,updateTrustAnchorsStore );
  }

  public PDFVerifyResult2 verifyPDF2(InputStream pdfDoc, String caStoreFolder, String caStoreName, ProtocolTypes useProtocol,boolean updateTrustAnchorsStore) throws VerifyException, TSLUpdateException, org.bouncycastle.cert.ocsp.OCSPException {
    PDFVerifyResult2 result = new PDFVerifyResult2();

    Security.addProvider(new BouncyCastleProvider());

    result.setStatus(VerifyStatus.PDF_VERIFY_ERROR);

    PdfReader reader = null;

    if (this.m_proxyHost != null) {
      this.m_useProxy = true;
      this.Log.info("Using Proxy: " + this.m_proxyUser + "@" + this.m_proxyHost + ":" + this.m_proxyPort);
    }
    try
    {
      reader = new PdfReader(pdfDoc);
      this.Log.info("File is a valid PDF");
      if(updateTrustAnchorsStore){
	      if (TrustedLists.updateTrustAnchorsStore(caStoreFolder, caStoreName, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
	        this.Log.info("updateTrustAnchorsStore: OK");
	      } else {
	        this.Log.error("updateTrustAnchorsStore: FAILED!");
	        result.setExceptionMessage("updateTrustAnchorsStore: FAILED!");
	
	        return result;
	      }
	    }else{
			this.Log.info("updateTrustAnchorsStore: SKIPP");
		}


      KeyStore anchors = loadCacertsKeyStore(caStoreFolder + caStoreName, null);
      AcroFields af = reader.getAcroFields();
      ArrayList names = af.getSignatureNames();

      if (names.size() == 0)
      {
        ArrayList blanks = af.getBlankSignatureNames();

        if (blanks.size() == 0) {
          this.Log.info("PDF has no signature fields.");
          result.setStatus(VerifyStatus.PDF_NOT_SIGNED);
          return result;
        }

        this.Log.info("PDF is not signed.");
        result.setStatus(VerifyStatus.PDF_NOT_SIGNED);
        return result;
      }

      for (int k = 0; k < names.size(); k++)
      {
        String sigName = (String)names.get(k);

        SignatureResult sigResult = verifySignature(caStoreFolder, useProtocol, anchors, af, sigName);

        result.getSignatures().add(sigResult);
      }

      Collection smallList = CollectionUtils.select(result.getSignatures(), new Predicate() {
        public boolean evaluate(Object o) {
          SignatureResult c = (SignatureResult)o;
          return !c.isChainValid();
        }
      });
      if (smallList.size() == 0)
        result.setStatus(VerifyStatus.PDF_VERIFY_ALL_VALID);
      else {
        result.setStatus(VerifyStatus.PDF_VERIFY_NOT_VALID);
      }
    }
    catch (InvalidPdfException e)
    {
      if (this.Log.getEffectiveLevel() == Level.INFO)
        this.Log.info(e.getMessage() + " - Increase log level for more info");
      else {
        e.printStackTrace();
      }
      result.setExceptionMessage(e.getMessage());
      result.setStatus(VerifyStatus.UNSUPPORTED_FILE);
    }
    catch (IOException e)
    {
      if (this.Log.getEffectiveLevel() == Level.INFO)
        this.Log.info(e.getMessage() + " - Increase log level for more info");
      else {
        e.printStackTrace();
      }
      result.setExceptionMessage(e.getMessage());
      result.setStatus(VerifyStatus.PDF_VERIFY_ERROR);
    }
    catch (TSLUpdateException e)
    {
      if (this.Log.getEffectiveLevel() == Level.INFO)
        this.Log.info(e.getMessage() + " - Increase log level for more info");
      else {
        e.printStackTrace();
      }
      result.setExceptionMessage(e.getMessage());
      throw new TSLUpdateException(e.getMessage());
    }
    catch (org.bouncycastle.cert.ocsp.OCSPException e) {
      if (this.Log.getEffectiveLevel() == Level.INFO)
        this.Log.info(e.getMessage() + " - Increase log level for more info");
      else {
        e.printStackTrace();
      }
      result.setExceptionMessage(e.getMessage());
      result.setStatus(VerifyStatus.PDF_VERIFY_ERROR);
      throw new org.bouncycastle.cert.ocsp.OCSPException(e.getMessage());
    } catch (Exception e)
    {//Altrimenti va in eccessione il processo di verifica della firma!!
        if (this.Log.getEffectiveLevel() == Level.INFO)
          this.Log.info(e.getMessage() + " - Increase log level for more info");
        else {
          e.printStackTrace();
        }
        result.setExceptionMessage(e.getMessage());
        result.setStatus(VerifyStatus.PDF_VERIFY_ERROR);
      }
    this.Log.info(result);
    return result;
  }

  public SignatureResult verifySignature(String caStoreFolder, ProtocolTypes useProtocol, KeyStore anchors, AcroFields af, String sigName)
    throws org.bouncycastle.cert.ocsp.OCSPException
  {
    boolean selfSigned = false;

    SignatureResult result = new SignatureResult();

    selfSigned = false;
    try
    {
      result.setSignatureFieldName(sigName);

      this.Log.info("Signature name: " + sigName);

      this.Log.info("Signature covers whole document: " + af.signatureCoversWholeDocument(sigName));

      this.Log.info("Document revision: " + af.getRevision(sigName) + " of " + af.getTotalRevisions());

      PdfPKCS7 pk = af.verifySignature(sigName, "BC");

      this.Log.info("Digest algorithm: " + pk.getHashAlgorithm());

      BasicOCSPResp ocsp = pk.getOcsp();

      List crls = new ArrayList();

      if (pk.getCRLs() != null) {
        for (CRL crl : pk.getCRLs()) {
          crls.add((X509CRL)crl);
        }

      }

      TimeStampToken ts = pk.getTimeStampToken();

      if ((!pk.isTsp()) && (ts != null)) {
        boolean impr = pk.verifyTimestampImprint();
        System.out.println("- Timestamp imprint verifies: " + impr);

        Calendar cal = pk.getTimeStampDate();

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH.mm.ss.SS");
        System.out.println("- Timestamp date: " + sdf.format(cal.getTime()));
      }
      else {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH.mm.ss.SS");
        System.out.println("- Signing time is from the clock of the signer's computer: " + sdf.format(pk.getSignDate().getTime()));
      }

      X509Certificate cert = pk.getSigningCertificate();

      CertStore certs = null;
      ArrayList certList = new ArrayList(Arrays.asList(pk.getCertificates()));
      try
      {
        certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
      }
      catch (InvalidAlgorithmParameterException e1) {
        e1.printStackTrace();
      }
      catch (NoSuchAlgorithmException e1) {
        e1.printStackTrace();
      }
      catch (NoSuchProviderException e1) {
        e1.printStackTrace();
      }

      X509Principal prin = new X509Principal(pk.getSigningCertificate().getSubjectX500Principal().getEncoded());

      this.Log.info("Certificate subject DN: " + prin.getName());


     Vector vectOU = prin.getValues(X509Principal.OU);
     Vector vectCN = prin.getValues(X509Principal.CN);
     Vector vectO = prin.getValues(X509Principal.O);
     
     
     String szSigner = "";
      if ((vectOU!= null && vectOU.size() > 0)&& (vectO!= null && vectO.size() > 0) && (vectCN!= null && vectCN.size() > 0) ){
    	  szSigner = "CN:" +vectCN.get(0) + ", O:" + vectO.get(0);
        szSigner = szSigner + ", OU:" + vectOU.get(0);
      } else if ( (vectO!= null && vectO.size() > 0) && (vectCN!= null && vectCN.size() > 0) ){
    	  szSigner = "CN:" +vectCN.get(0) + ", O:" + vectO.get(0);
      }else{
    	  this.Log.warn("*** PDF Certificate OU or O non presente***");
      }

      result.setSigner(szSigner);
      this.Log.info("Certificate Signer: " + result.getSigner());
      HashMap policies = getCertificatePolicies(cert);

      result.setSignerCertificatePolicies(policies);

      if (policies.size() > 0)
        for (Iterator iterator = policies.entrySet().iterator(); iterator.hasNext(); ) {
          Map.Entry object = (Map.Entry)iterator.next();

          this.Log.info("Certificate Policy - OID: " + object.getKey() + " VALUES: " + object.getValue());
        }
      else {
        this.Log.info("No Certificate Policies found");
      }

      result.setSignerNotAfter(cert.getNotAfter());
      try
      {
        if (isSelfSigned(pk.getSigningCertificate())) {
          this.Log.warn("*** PDF Certificate is self-signed ***");
          selfSigned = true;
        }
      }
      catch (CertificateException e2) {
        e2.printStackTrace();
      }
      catch (NoSuchAlgorithmException e2) {
        e2.printStackTrace();
      }
      catch (NoSuchProviderException e2) {
        e2.printStackTrace();
      }

      try
      {
        pk.getSigningCertificate().checkValidity();
        result.setSignerCertificateTimeValid(true);
        this.Log.info("User Certificate is time valid");
      }
      catch (CertificateExpiredException e) {
        this.Log.warn("User Certificate is expired!");
      }
      catch (CertificateNotYetValidException e)
      {
        this.Log.warn("User Certificate not yet valid!");
      }

      boolean bVerify = pk.verify();
      this.Log.info("Document modified: " + (!bVerify));
      result.setContentValid(bVerify);

      if (!result.isSignerCertificateTimeValid()) return result;
      if (selfSigned) {
        try {
          anchors.setCertificateEntry(cert.getSerialNumber().toString(36), cert);
        }
        catch (KeyStoreException e) {
          result.setExceptionMessage(e.getMessage());
          return result;
        }
      }
      else
      {
        anchors.setCertificateEntry(cert.getSerialNumber().toString(36), cert);

        this.Log.info("Using " + anchors.size() + " trusted identities - ");

        X509CertSelector selector = new X509CertSelector();
        selector.setCertificate(cert);
        //this.Log.info("setCertificate:"+cert);
        //this.Log.info("anchors:"+anchors);
        //this.Log.info("selector:"+selector);
        //SE C'E' UNA CA NELLA CHAIN INTENA DEL PDF DEVO AGGIUNGERLA ALLE ANCHORS...
        //COSI' PERO' C'E' UN BUCO DI SICUREZZA NEL SENSO CHE LA CA VIENE TRUSTATA SEMPRE
        //IN REALTA' I PDF CHE ARRIVANO COSI' NON DOVREBBERO ESSERE TRUSTATI
        java.security.cert.Certificate[] chain = pk.getSignCertificateChain();
        X509Certificate caCert = getCAFromCertificates(chain);
        if(caCert != null){
        	anchors.setCertificateEntry("Issuer CA", caCert);
        }
        PKIXBuilderParameters params = new PKIXBuilderParameters(anchors, selector);

        params.addCertStore(certs);
        params.setRevocationEnabled(false);
        this.Log.info("setRevocationEnabled false");

        Date dt = new SimpleDateFormat("dd/MM/yyyy").parse("02/08/2009");
        
        	
        CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
        PKIXCertPathBuilderResult res = (PKIXCertPathBuilderResult)builder.build(params);

        TrustAnchor CA = res.getTrustAnchor();
         this.Log.info("CA Certificate getTrustAnchor");

        result.setAnchorNotAfter(CA.getTrustedCert().getNotAfter());
        try
        {
          CA.getTrustedCert().checkValidity();
          result.setAnchorCertificateTimeValid(true);
          this.Log.info("CA Certificate is time valid");
        }
        catch (CertificateExpiredException e) {
          this.Log.warn("CA Certificate is expired!");
        }
        catch (CertificateNotYetValidException e)
        {
          this.Log.warn("CA Certificate not yet valid!");
        }

        if (!result.isAnchorCertificateTimeValid()) {
          return result;
        }

        if (isCertificateRevoked(CA.getTrustedCert(), CA.getTrustedCert(), caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
          result.setAnchorCertificateRevoked(true);
          this.Log.warn("CA Certificate is revoked!");
        } else {
          this.Log.warn("CA Certificate NOT revoked");
        }

        if (isCertificateRevoked(CA.getTrustedCert(), cert, caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
          result.setSignerCertificateRevoked(true);
          this.Log.warn("Signer Certificate is revoked!");
        } else {
          this.Log.warn("Signer Certificate NOT revoked");
        }

        Object cp = res.getCertPath();

        this.Log.info("Certificate chain validated successfully -> " + cp.getClass().getName());

        result.setChainValid(true);
      }
    }
    catch (SignatureException e)
    {
    //  e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (CertStoreException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (CertificateParsingException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (ParseException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (CertPathBuilderException e) {
      this.Log.warn(e.getMessage());
      result.setExceptionMessage(e.getMessage());
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    catch (GeneralSecurityException e) {
      e.printStackTrace();
    }catch (Exception e) {
        e.printStackTrace();
        result.setExceptionMessage("Eccezione generica durante la verifica della firma");
     }


    return result;
  }
  
  
  X509Certificate getCAFromCertificates(java.security.cert.Certificate[] certs){
   if(certs==null) return null;
    try{
	   for (int i = 0; i < certs.length; i++) {
    		java.security.cert.Certificate certificate = certs[i];

    		boolean[] keyUsage = ((X509Certificate)certificate).getKeyUsage();
    		if(keyUsage==null) return null;
    		if ( keyUsage[5] ) {
    			return (X509Certificate)certificate;
    		}else {
    			// User certificate
    		}
    	}
    }catch(Exception e){
    	System.out.println("Attenzione: getCAFromCertificates in errore");
    	return null;
    }
    	return null;
    }

  public PDFVerifyResult verifyPDF(Document pdfDoc, String caStoreFolder, String caStoreName, ProtocolTypes useProtocol) throws VerifyException, TSLUpdateException, org.bouncycastle.cert.ocsp.OCSPException, GeneralSecurityException {
    return verifyPDF(pdfDoc.getInputStream(), caStoreFolder, caStoreName, useProtocol);
  }

  public PDFVerifyResult verifyPDF(InputStream pdfDoc, String caStoreFolder, String caStoreName, ProtocolTypes useProtocol) throws VerifyException, TSLUpdateException, org.bouncycastle.cert.ocsp.OCSPException, GeneralSecurityException {
    PDFVerifyResult result = new PDFVerifyResult();
    boolean selfSigned = false;

    Security.addProvider(new BouncyCastleProvider());

    result.setVerifySignatureResult(VerifySignatureResult.PDF_VERIFY_ERROR);

    PdfReader reader = null;

    if (this.m_proxyHost != null) {
      this.m_useProxy = true;
      this.Log.info("Using Proxy: " + this.m_proxyUser + "@" + this.m_proxyHost + ":" + this.m_proxyPort);
    }
    try
    {
      reader = new PdfReader(pdfDoc);
      this.Log.info("File is a valid PDF");

      if (TrustedLists.updateTrustAnchorsStore(caStoreFolder, caStoreName, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
        this.Log.info("updateTrustAnchorsStore: OK");
      } else {
        this.Log.error("updateTrustAnchorsStore: FAILED!");
        result.setExceptionMessage("updateTrustAnchorsStore: FAILED!");
        return result;
      }

      KeyStore anchors = loadCacertsKeyStore(caStoreFolder + caStoreName, null);

      AcroFields af = reader.getAcroFields();

      ArrayList names = af.getSignatureNames();

      for (int k = 0; k < names.size(); k++)
      {
        selfSigned = false;

        String sigName = (String)names.get(k);

        this.Log.info("Signature name: " + sigName);

        this.Log.info("Signature covers whole document: " + af.signatureCoversWholeDocument(sigName));

        this.Log.info("Document revision: " + af.getRevision(sigName) + " of " + af.getTotalRevisions());

        PdfPKCS7 pk = af.verifySignature(sigName, "BC");
        X509Certificate cert = pk.getSigningCertificate();

        CertStore certs = null;
        ArrayList certList = new ArrayList(Arrays.asList(pk.getCertificates()));
        try
        {
          certs = CertStore.getInstance("Collection", new CollectionCertStoreParameters(certList), "BC");
        }
        catch (InvalidAlgorithmParameterException e1) {
          e1.printStackTrace();
        }
        catch (NoSuchAlgorithmException e1) {
          e1.printStackTrace();
        }
        catch (NoSuchProviderException e1) {
          e1.printStackTrace();
        }

        X509Principal prin = new X509Principal(pk.getSigningCertificate().getSubjectX500Principal().getEncoded());

        this.Log.info("Certificate subject DN: " + prin.getName());

        String szSigner = "CN:" + prin.getValues(X509Principal.CN).get(0) + ", O:" + prin.getValues(X509Principal.O).get(0);

        Vector vect = prin.getValues(X509Principal.OU);

        if (vect.size() > 0) {
          szSigner = szSigner + ", OU:" + prin.getValues(X509Principal.OU).get(0);
        }

        result.setSigner(szSigner);

        this.Log.info("Signer: " + result.getSigner());

        result.setSignerNotAfter(cert.getNotAfter());
        try
        {
          if (isSelfSigned(pk.getSigningCertificate())) {
            this.Log.warn("*** PDF Certificate is self-signed ***");
            selfSigned = true;
          }
        }
        catch (CertificateException e2) {
          e2.printStackTrace();
        }
        catch (NoSuchAlgorithmException e2) {
          e2.printStackTrace();
        }
        catch (NoSuchProviderException e2) {
          e2.printStackTrace();
        }

        try
        {
          pk.getSigningCertificate().checkValidity();

          result.setSignerCertificateTimeValid(true);

          this.Log.info("User Certificate is time valid");
        }
        catch (CertificateExpiredException e) {
          this.Log.warn("User Certificate is expired!");
        }
        catch (CertificateNotYetValidException e)
        {
          this.Log.warn("User Certificate not yet valid!");
        }

        boolean bVerify = pk.verify();

        this.Log.info("Document modified: " + (!bVerify));

        result.setContentValid(bVerify);

        if (!result.isSignerCertificateTimeValid()) return result;

        if (selfSigned) {
          try {
            anchors.setCertificateEntry(cert.getSerialNumber().toString(36), cert);
          }
          catch (KeyStoreException e) {
            result.setExceptionMessage(e.getMessage());
            return result;
          }
        }
        else
        {
          anchors.setCertificateEntry(cert.getSerialNumber().toString(36), cert);

          this.Log.info("Using " + anchors.size() + " trusted identities");

          X509CertSelector selector = new X509CertSelector();
          selector.setCertificate(cert);

          PKIXBuilderParameters params = new PKIXBuilderParameters(anchors, selector);

          params.addCertStore(certs);
          params.setRevocationEnabled(false);

          CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");
          PKIXCertPathBuilderResult res = (PKIXCertPathBuilderResult)builder.build(params);

          TrustAnchor CA = res.getTrustAnchor();

          result.setAnchorNotAfter(CA.getTrustedCert().getNotAfter());
          try
          {
            CA.getTrustedCert().checkValidity();

            result.setAnchorCertificateTimeValid(true);

            this.Log.info("CA Certificate is time valid");
          }
          catch (CertificateExpiredException e) {
            this.Log.warn("CA Certificate is expired!");
          }
          catch (CertificateNotYetValidException e)
          {
            this.Log.warn("CA Certificate not yet valid!");
          }

          if (!result.isAnchorCertificateTimeValid()) {
            return result;
          }

          if (isCertificateRevoked(CA.getTrustedCert(), CA.getTrustedCert(), caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
            result.setAnchorCertificateRevoked(true);
            this.Log.warn("CA Certificate is revoked!");
          } else {
            this.Log.warn("CA Certificate NOT revoked");
          }

          if (isCertificateRevoked(CA.getTrustedCert(), cert, caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
            result.setSignerCertificateRevoked(true);
            this.Log.warn("Signer Certificate is revoked!");
          } else {
            this.Log.warn("Signer Certificate NOT revoked");
          }

          Object cp = res.getCertPath();

          this.Log.info("Certificate chain validated successfully -> " + cp.getClass().getName());

          result.setChainValid(true);
          result.setVerifySignatureResult(VerifySignatureResult.SUCCESS);
        }
      }

    }
    catch (InvalidPdfException e)
    {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      result.setVerifySignatureResult(VerifySignatureResult.UNSUPPORTED_FILE);
      throw new VerifyException(e.getMessage());
    }
    catch (IOException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      result.setVerifySignatureResult(VerifySignatureResult.UNSUPPORTED_FILE);
      throw new VerifyException(e.getMessage());
    }
    catch (SignatureException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (CertStoreException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (CertificateParsingException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (InvalidAlgorithmParameterException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (CertPathBuilderException e)
    {
      this.Log.warn(e.getMessage());
      result.setExceptionMessage(e.getMessage());
      throw new VerifyException(e.getMessage());
    }
    catch (org.bouncycastle.cert.ocsp.OCSPException e) {
      e.printStackTrace();
      result.setExceptionMessage(e.getMessage());
      throw new org.bouncycastle.cert.ocsp.OCSPException(e.getMessage());
    }catch (Exception e)
    {
        this.Log.warn(e.getMessage());
        result.setExceptionMessage(e.getMessage());
        throw new VerifyException(e.getMessage());
    }

    return result;
  }

  public OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
    throws org.bouncycastle.cert.ocsp.OCSPException, org.bouncycastle.ocsp.OCSPException, CertificateEncodingException, OperatorCreationException, IOException
  {
    CertificateID id = new CertificateID(new JcaDigestCalculatorProviderBuilder().setProvider("BC").build().get(CertificateID.HASH_SHA1), new X509CertificateHolder(issuerCert.getEncoded()), serialNumber);

    OCSPReqBuilder OCSPBuilder = new OCSPReqBuilder();

    OCSPBuilder.addRequest(id);

    BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());

    Extension ext = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, true, new DEROctetString(nonce.toByteArray()));
    OCSPBuilder.setRequestExtensions(new Extensions(new Extension[] { ext }));

    return OCSPBuilder.build();
  }

  private boolean isSelfSigned(X509Certificate cert)
    throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException
  {
    try
    {
      PublicKey key = cert.getPublicKey();

      cert.verify(key);

      return true;
    }
    catch (SignatureException sigEx)
    {
      return false;
    }
    catch (InvalidKeyException keyEx)
    {
    }

    return false;
  }

  public Vector<ASN1String> getCrlDistributionPoint(X509Certificate certificate)
    throws CertificateParsingException, IOException
  {
    byte[] extension = certificate.getExtensionValue(X509Extension.cRLDistributionPoints.getId());

    if (extension != null)
    {
      this.Log.info("CRL Distribution Points Extension:");

      CRLDistPoint distPoints = CRLDistPoint.getInstance(X509ExtensionUtil.fromExtensionValue(extension));

      DistributionPoint[] points = distPoints.getDistributionPoints();

      Vector urls = new Vector();

      for (int i = 0; i != points.length; i++)
      {
        DistributionPoint dp = points[i];
        DistributionPointName dpn = dp.getDistributionPoint();

        GeneralNames gns = (GeneralNames)points[i].getDistributionPoint().getName();
        GeneralName[] vgn = gns.getNames();
        for (int j = 0; j < vgn.length; j++)
        {
          if (vgn[j].getTagNo() == 6)
          {
            ASN1String s = (ASN1String)vgn[j].getName();
            this.Log.info("\t" + s.getString());
            urls.add(s);
          }
        }
      }

      return urls;
    }
    return null;
  }

  private Vector getDERValue(ASN1Primitive derObj) {
    if ((derObj instanceof DERSequence)) {
      Vector ret = new Vector();
      DERSequence seq = (DERSequence)derObj;
      Enumeration en = seq.getObjects();
      while (en.hasMoreElements()) {
        ASN1Primitive nestedObj = (ASN1Primitive)en.nextElement();
        Vector appo = getDERValue(nestedObj);
        if (appo != null) {
          ret.addAll(appo);
        }
      }
      return ret;
    }

    if ((derObj instanceof DERTaggedObject)) {
      DERTaggedObject derTag = (DERTaggedObject)derObj;
      if ((derTag.isExplicit()) && (!derTag.isEmpty())) {
        ASN1Primitive nestedObj = derTag.getObject();
        Vector ret = getDERValue(nestedObj);
        return ret;
      }
      DEROctetString derOct = (DEROctetString)derTag.getObject();
      String val = new String(derOct.getOctets());
      Vector ret = new Vector();
      ret.add(val);
      return ret;
    }

    return null;
  }

  public KeyStore loadCacertsKeyStore(String storePath, String provider) {
    File file = new File(storePath);

    this.Log.info("CA ANCHORS PATH LOADED FROM: " + file.getPath());

    FileInputStream fin = null;
    try {
      fin = new FileInputStream(file);
      KeyStore k;
      if (provider == null)
        k = KeyStore.getInstance("JKS");
      else
        k = KeyStore.getInstance("JKS", provider);
      k.load(fin, null);
      return k;
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    catch (CertificateException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
        e.printStackTrace();
     }
    finally {
      try {
        if (fin != null) fin.close();  } catch (Exception localException7) {  }

    }
    return null;
  }

  public boolean isCertificateRevoked(X509Certificate TrustedAnchor, X509Certificate certificate, String caStoreFolder, ProtocolTypes useProtocol, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws CertStoreException, IOException, CertificateParsingException, NoSuchProviderException, org.bouncycastle.cert.ocsp.OCSPException {
    String OCSPResponder = "";

    Vector OCSPs = getOCSPResponder(certificate);

    if (OCSPs != null) {
      Object[] sOCSPs = OCSPs.toArray();

      OCSPResponder = (String)sOCSPs[0];

      this.Log.info("Found OCSP responder for certificate at " + OCSPResponder);
      try
      {
        OCSPReq req = generateOCSPRequest(TrustedAnchor, certificate.getSerialNumber());

        OCSPResp resp = getOCSPResponseFromHttp(req.getEncoded(), OCSPResponder, useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd);

        if (resp == null) {
          throw new org.bouncycastle.cert.ocsp.OCSPException("OCSP Response fault see log for details");
        }

        if (resp.getStatus() != 0)
        {
          this.Log.info("Invalid OCSP response status: " + resp.getStatus());
        }

        BasicOCSPResp basicResponse = (BasicOCSPResp)resp.getResponseObject();

        if (basicResponse != null) {
          SingleResp[] responses = basicResponse.getResponses();
          if (responses.length == 1) {
            SingleResp singleResp = responses[0];
            Object status = singleResp.getCertStatus();
            if (status == CertificateStatus.GOOD) {
              this.Log.info("OCSP Status is GOOD");
              return false;
            }
            if ((status instanceof RevokedStatus)) {
              throw new IOException("OCSP Status is revoked!");
            }

            throw new IOException("OCSP Status is unknown!");
          }

        }

        BasicOCSPResp brep = (BasicOCSPResp)resp.getResponseObject();

        JcaX509CertificateConverter conv = new JcaX509CertificateConverter();

        conv.setProvider("BC");

        boolean verify = brep.isSignatureValid((ContentVerifierProvider)conv);

        if (!verify) {
          this.Log.error("ERRORE OCSP VALIDAZIONE");
        }

        SingleResp[] singleResps = brep.getResponses();

        for (int i = 0; i < singleResps.length; i++) {
          SingleResp singleResp = singleResps[i];
          Object certStatus = singleResp.getCertStatus();

          if (certStatus != null)
          {
            this.Log.error("Status is not null. 'null' is the success result " + certStatus.toString());
          }
        }
      }
      catch (org.bouncycastle.cert.ocsp.OCSPException e)
      {
        e.printStackTrace();
      }
      catch (CertificateEncodingException e) {
        e.printStackTrace();
      }
      catch (OperatorCreationException e) {
        e.printStackTrace();
      }
      catch (org.bouncycastle.ocsp.OCSPException e) {
        e.printStackTrace();
      }

    }
    else
    {
      Vector urls = getCrlDistributionPoint(certificate);

      Collection crlCollection = new ArrayList();

      byte[] val = (byte[])null;
      Hashtable env = new Hashtable();

      boolean bDownloadedCERTCRL = false;

      if (urls != null) {
        Object[] objs = urls.toArray();

        for (int j = 0; j < objs.length; j++) {
          String crlURLString = ((DERIA5String)objs[j]).getString();

          if (((crlURLString.contains("ldap://")) && (useProtocol == ProtocolTypes.Ldap)) || 
            ((crlURLString.contains("http://")) && (useProtocol == ProtocolTypes.Http)) || 
            (useProtocol == ProtocolTypes.Both))
          {
            try
            {
              X509CRL crl = createCRLFromUrl(crlURLString, useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd);
              Date dt = crl.getNextUpdate();

              this.Log.info("Downloaded CERT CRL: " + crlURLString);
              bDownloadedCERTCRL = true;
              crlCollection.add(crl);

              if (crl.isRevoked(certificate)) {
                this.Log.warn("USER CERTIFICATE REVOKED...........");
                return true;
              }
              this.Log.info("USER CERTIFICATE NOT REVOKED.......");
            }
            catch (NamingException e)
            {
              this.Log.error("Unable to download CERT CRL: " + crlURLString + " -> " + e.getExplanation()); continue;
            }
            catch ( Exception e)
            {
              this.Log.error("Unable to download CERT CRL: " + crlURLString + " -> " + e.getLocalizedMessage()); continue;
            }
          }
          else
            this.Log.warn("Protocol not allowed skipping CERT CRL download: " + crlURLString);
        }
      }
      else
      {
        this.Log.warn("CRL extension not found in Certificate!");
      }

    }

    return false;
  }

  public Vector getOCSPResponder(X509Certificate certificate) throws CertificateParsingException
  {
    try {
      byte[] val1 = certificate.getExtensionValue(X509Extension.authorityInfoAccess.getId());

      if (val1 == null) return null;

      ASN1InputStream oAsnInStream = new ASN1InputStream(new ByteArrayInputStream(val1));
      ASN1Primitive derObj = oAsnInStream.readObject();
      DEROctetString dos = (DEROctetString)derObj;
      byte[] val2 = dos.getOctets();
      ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(val2));
      ASN1Primitive derObj2 = oAsnInStream2.readObject();
      return getDERValue(derObj2);
    }
    catch (Exception e)
    {
      throw new CertificateParsingException(e.toString());
    }
  }

  public OCSPResp getOCSPResponseFromHttp(byte[] ocspPackage, String url, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws IOException
  {
    InputStream crlInputStream = null;
    HttpURLConnection conn = null;
    try
    {
      URL urla = new URL(url);

      if (useProxy) {
        Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));

        SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));

        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

        conn = (HttpURLConnection)urla.openConnection(proxy);
      } else {
        conn = (HttpURLConnection)urla.openConnection();
      }

      conn.setRequestProperty("Content-Type", "application/ocsp-request");
      conn.setRequestProperty("Accept", "application/ocsp-response");
      conn.setDoOutput(true);

      OutputStream out = conn.getOutputStream();

      DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));

      dataOut.write(ocspPackage);

      dataOut.flush();
      dataOut.close();

      if (conn.getResponseCode() / 100 != 2) {
        this.Log.error("Invalid HTTP response, Code: " + conn.getResponseCode() + ", Msg: " + conn.getResponseMessage());
        return null;
      }

      InputStream in = (InputStream)conn.getContent();
      return new OCSPResp(in);
    }
    catch (MalformedURLException e1)
    {
      e1.printStackTrace();
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }
    catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  private X509CRL createCRLFromUrl(String url, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd)
  {
    InputStream crlInputStream = null;

    if (url.toLowerCase().contains("ldap://")) {
      try
      {
        byte[] val = (byte[])null;

        Hashtable env = new Hashtable();

        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", proxyPort);
        System.setProperty("http.proxyUser", proxyUsr);
        System.setProperty("http.proxyPassword", proxyPwd);

        Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));

        env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
        env.put("java.naming.provider.url", url);
        env.put("java.naming.security.authentication", "none");

        DirContext ctx = new InitialDirContext(env);

        Attributes avals = ctx.getAttributes("");

        Attribute aval = avals.get("certificateRevocationList;binary");
        val = (byte[])aval.get();

        if ((val == null) || (val.length == 0))
        {
          this.Log.error("Unable to get crl from ldap ");

          return null;
        }

        crlInputStream = new ByteArrayInputStream(val);
      }
      catch (NamingException e)
      {
        e.printStackTrace();
      }

    }
    else
    {
      try
      {
        Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));

        SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));

        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

        URL urla = new URL(url);

        URLConnection conn = urla.openConnection(proxy);

        crlInputStream = conn.getInputStream();
      }
      catch (MalformedURLException e1)
      {
        e1.printStackTrace();
      }
      catch (IOException e1) {
        this.Log.error(e1.getMessage());
      }

    }

    CertificateFactory cf = null;
    try {
      cf = CertificateFactory.getInstance("X.509");
    }
    catch (CertificateException ce) {
      ce.printStackTrace();
    }
    try
    {
      X509CRL crl = (X509CRL)cf.generateCRL(crlInputStream);
      this.Log.info("<-- CRL Issuer\t--> " + crl.getIssuerDN());
      this.Log.info("<-- CRL Effective\t--> " + crl.getThisUpdate());
      this.Log.info("<-- CRL NextDate\t--> " + crl.getNextUpdate());
      crlInputStream.close();

      return crl;
    }
    catch (Exception e) {
      this.Log.error(e.getMessage());
    }

    return null;
  }

  private HashMap<String, String> getCertificatePolicies(X509Certificate certificate) throws IOException
  {
    HashMap tuple = new HashMap();

    byte[] bValue = certificate.getExtensionValue(X509Extension.certificatePolicies.getId());

    if (bValue == null) return tuple;

    DEROctetString oct = (DEROctetString)new ASN1InputStream(new ByteArrayInputStream(bValue)).readObject();

    ASN1Sequence pSeq = (ASN1Sequence)new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject();

    StringBuffer sb = null;

    int i = 0; for (int len = pSeq.size(); i < len; i++)
    {
      sb = new StringBuffer();

      PolicyInformation pi = PolicyInformation.getInstance(pSeq.getObjectAt(i));
      ASN1Sequence pQuals;
      if ((pQuals = pi.getPolicyQualifiers()) != null)
      {
        int j = 0; for (int plen = pQuals.size(); j < plen; j++)
        {
          ASN1Sequence pqi = (ASN1Sequence)pQuals.getObjectAt(j);
          String pqId = ((DERObjectIdentifier)pqi.getObjectAt(0)).getId();

          if (pQuals.size() > 0)
          {
            ASN1Encodable d = pqi.getObjectAt(1);

            if (pqId.equals("1.3.6.1.5.5.7.2.1"))
            {
              sb.append(((ASN1String)d).getString());
              sb.append('\n');
            }
            else if (pqId.equals("1.3.6.1.5.5.7.2.2"))
            {
              ASN1Sequence un = (ASN1Sequence)d;

              int k = 0; for (int dlen = un.size(); k < dlen; k++) {
                ASN1Encodable de = un.getObjectAt(k);

                if ((de instanceof ASN1String))
                {
                  sb.append(stringify(de));
                  sb.append('\n');
                }
                else if ((de instanceof ASN1Sequence))
                {
                  ASN1Sequence nr = (ASN1Sequence)de;
                  String orgstr = stringify(nr.getObjectAt(0));
                  ASN1Sequence nrs = (ASN1Sequence)nr.getObjectAt(1);
                  StringBuffer nrstr = new StringBuffer();
                  int m = 0; for (int nlen = nrs.size(); m < nlen; m++) {
                    nrstr.append(stringify(nrs.getObjectAt(m)));
                    if (m != nlen - 1) {
                      nrstr.append(", ");
                    }

                  }

                  sb.append(new String[] { orgstr });
                  sb.append("\n");

                  sb.append(new Object[] { nrstr });
                  sb.append('\n');
                }

              }

            }
            else
            {
              sb.append(stringify(d));
              sb.append('\n');
            }
          }
        }
      }

      tuple.put(pi.getPolicyIdentifier().getId().toString(), sb.toString());
    }

    return tuple;
  }

  private static String stringify(Object obj) {
    if ((obj instanceof ASN1String)) {
      return ((ASN1String)obj).getString();
    }
    if (((obj instanceof DERInteger)) || ((obj instanceof byte[]))) {
      return convertToHexString(obj);
    }
    if ((obj instanceof ASN1TaggedObject)) {
      ASN1TaggedObject tagObj = (ASN1TaggedObject)obj;

      return "[" + tagObj.getTagNo() + "] " + stringify(tagObj.getObject());
    }
    if ((obj instanceof ASN1Sequence)) {
      ASN1Sequence aObj = (ASN1Sequence)obj;
      StringBuffer tmp = new StringBuffer("[");
      int i = 0; for (int len = aObj.size(); i < len; i++) {
        tmp.append(stringify(aObj.getObjectAt(i)));
        if (i != len - 1) {
          tmp.append(", ");
        }
      }
      return "]";
    }

    String hex = null;
    try {
      Method method = obj.getClass().getMethod("getOctets", null);
      hex = convertToHexString(method.invoke(obj, null));
    }
    catch (Exception localException)
    {
    }
    if ((hex == null) && (obj != null)) {
      hex = obj.toString();
    }
    return hex;
  }

  private static String convertToHexString(Object obj)
  {
    BigInteger bigInt;
    if ((obj instanceof BigInteger)) {
      bigInt = (BigInteger)obj;
    }
    else
    {
      if ((obj instanceof byte[])) {
        bigInt = new BigInteger(1, (byte[])obj);
      }
      else
      {
        if ((obj instanceof DERInteger)) {
          bigInt = ((DERInteger)obj).getValue();
        }
        else
          throw new IllegalArgumentException("Don't know how to convert " + obj.getClass().getName() + 
            " to a hex string");
      }
    }
    StringBuffer strBuff = new StringBuffer(bigInt.toString(16).toUpperCase());

    if (strBuff.length() > 4) {
      for (int iCnt = 4; iCnt < strBuff.length(); iCnt += 5) {
        strBuff.insert(iCnt, ' ');
      }
    }

    return strBuff.toString();
  }

  private X509CRL createCRLFromUrl(String url, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws NamingException
  {
    InputStream crlInputStream = null;
    URLConnection conn = null;

    if (url.toLowerCase().contains("ldap://"))
    {
      byte[] val = (byte[])null;

      Hashtable env = new Hashtable();

      if (useProxy) {
        System.setProperty("http.proxyHost", proxyHost);
        System.setProperty("http.proxyPort", proxyPort);

        Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));
      }

      env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
      env.put("java.naming.provider.url", url);
      env.put("java.naming.security.authentication", "none");

      DirContext ctx = new InitialDirContext(env);

      Attributes avals = ctx.getAttributes("");

      Attribute aval = avals.get("certificateRevocationList;binary");
      val = (byte[])aval.get();

      if ((val == null) || (val.length == 0))
      {
        this.Log.error("Unable to get crl from ldap ");

        return null;
      }

      crlInputStream = new ByteArrayInputStream(val);
    }
    else
    {
      try
      {
        URL urla = new URL(url);
        if (useProxy) {
        this.Log.info("Use Proxy:" + proxyHost +":"+proxyPort+"-"+proxyUsr);
          Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));
          SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));
          Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
          conn = urla.openConnection(proxy);
        } else {
          conn = urla.openConnection();
        }

        crlInputStream = conn.getInputStream();
      }
      catch (MalformedURLException e1)
      {
        //e1.printStackTrace();
      }
      catch (IOException e1) {
       // e1.printStackTrace();
      }

    }

    if (crlInputStream == null) return null;

    CertificateFactory cf = null;
    try
    {
      cf = CertificateFactory.getInstance("X.509");
    }
    catch (CertificateException ce) {
      ce.printStackTrace();
    }
    try
    {
      X509CRL crl = (X509CRL)cf.generateCRL(crlInputStream);
      this.Log.info("<-- CRL Issuer\t--> " + crl.getIssuerDN());
      this.Log.info("<-- CRL Effective\t--> " + crl.getThisUpdate());
      this.Log.info("<-- CRL NextDate\t--> " + crl.getNextUpdate());
      crlInputStream.close();

      return crl;
    }
    catch (Exception e) {
     e.printStackTrace();
    }

    return null;
  }

  private boolean isCRLvalid(X509CRL crl, X509Certificate ca)
  {
    try
    {
      crl.verify(ca.getPublicKey());
    } catch (Exception e) {
      this.Log.error("crl.verify failed = " + e);
      return false;
    }

    return true;
  }

  private boolean isCRLexpired(X509CRL crl)
  {
    Calendar today = Calendar.getInstance();
    try
    {
      this.Log.info("crl.getNextUpdate() = " + crl.getNextUpdate());

      if (today.getTime().after(crl.getNextUpdate()))
        return true;
    }
    catch (Exception e) {
      this.Log.error("crl expiration check failed = " + e);
      return true;
    }
    return false;
  }

  private void addCertsFromSet(List certs, ASN1Set certSet, Provider provider)
    throws CMSException
  {
    CertificateFactory cf = null;
    try
    {
      if (provider == null)
      {
        cf = CertificateFactory.getInstance("X.509");
      }
    }
    catch (CertificateException ex)
    {
      throw new CMSException("can't get certificate factory.", ex);
    }
    Enumeration e = certSet.getObjects();

    while (e.hasMoreElements())
    {
      try
      {
        ASN1Primitive obj = ((ASN1Encodable)e.nextElement()).toASN1Primitive();

        if ((obj instanceof ASN1Sequence))
        {
          certs.add(cf.generateCertificate(
            new ByteArrayInputStream(obj.getEncoded())));
        }
      }
      catch (IOException ex)
      {
        throw new CMSException(
          "can't re-encode certificate!", ex);
      }
      catch (CertificateException ex)
      {
        throw new CMSException(
          "can't re-encode certificate!", ex);
      }
    }
  }

  private void addCRLsFromSet(List crls, ASN1Set certSet, Provider provider)
    throws CMSException
  {
    try
    {
      CertificateFactory cf;
      if (provider != null)
      {
        cf = CertificateFactory.getInstance("X.509", provider);
      }
      else
      {
        cf = CertificateFactory.getInstance("X.509");
      }
    }
    catch (CertificateException ex)
    {
      CertificateFactory cf;
      throw new CMSException("can't get certificate factory.", ex);
    }
    CertificateFactory cf = null;
    Enumeration e = certSet.getObjects();

    while (e.hasMoreElements())
    {
      try
      {
        ASN1Primitive obj = ((ASN1Encodable)e.nextElement()).toASN1Primitive();

        crls.add(cf.generateCRL(
          new ByteArrayInputStream(obj.getEncoded())));
      }
      catch (IOException localIOException)
      {
      }
      catch (CRLException ex)
      {
        throw new CMSException("can't re-encode CRL!", ex);
      }
    }
  }

  private CertStore createCertStore(String type, Provider provider, ASN1Set certSet, ASN1Set crlSet)
    throws CMSException, NoSuchAlgorithmException
  {
    List certsAndcrls = new ArrayList();

    if (certSet != null)
    {
      addCertsFromSet(certsAndcrls, certSet, provider);
    }

    if (crlSet != null)
    {
      addCRLsFromSet(certsAndcrls, crlSet, provider);
    }

    try
    {
      if (provider != null)
      {
        return CertStore.getInstance(type, new CollectionCertStoreParameters(certsAndcrls), provider);
      }

      return CertStore.getInstance(type, new CollectionCertStoreParameters(certsAndcrls));
    }
    catch (InvalidAlgorithmParameterException e)
    {
      throw new CMSException("can't setup the CertStore", e);
    }
  }

}