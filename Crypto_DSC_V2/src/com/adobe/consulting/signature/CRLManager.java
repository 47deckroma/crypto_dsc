package com.adobe.consulting.signature;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509CRL;
import java.util.ArrayList;
import java.util.Collection;

public class CRLManager
{
  public static synchronized CertStore getCRLStore(String path)
  {
    Collection crlList = new ArrayList();

    File folder = new File(path);
    File[] listOfFiles = folder.listFiles();

    if (listOfFiles != null)
    {
      for (int i = 0; i < listOfFiles.length; i++) {
        if (listOfFiles[i].isFile()) {
          String name = listOfFiles[i].getName();

          if ((name.endsWith(".der")) || (name.endsWith(".DER"))) {
            try
            {
              FileInputStream fis = new FileInputStream(new File(name));

              CertificateFactory cf = CertificateFactory.getInstance("X509CRL");

              X509CRL crl = (X509CRL)cf.generateCRLs(fis);

              crlList.add(crl);

              fis.close();
            }
            catch (FileNotFoundException e)
            {
              e.printStackTrace();
            }
            catch (CertificateException e) {
              e.printStackTrace();
            }
            catch (CRLException e) {
              e.printStackTrace();
            }
            catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
      }
    }

    try
    {
      return CertStore.getInstance("Collection", new CollectionCertStoreParameters(crlList), "BC");
    }
    catch (InvalidAlgorithmParameterException e)
    {
      e.printStackTrace();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }

    return null;
  }

  public static boolean persistCRL(String basePath, String name)
  {
    return false;
  }
}