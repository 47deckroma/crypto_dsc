package com.adobe.consulting.signature;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.adobe.consulting.crypto.custom.SSLExcludeCipherConnectionHelper;

import sun.misc.BASE64Decoder;

public class TrustedLists
{
  static Logger Log = Logger.getLogger(TrustedLists.class);
  
  public static void main(String[] args) {
	  try {
		TrustedLists.updateTrustAnchorsStore("D:\\", "", true, "itaca-prod.utenze.bankit.it", "8080", "iw53434", "livecycle05$");
	} catch (TSLUpdateException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
  
  
  public static synchronized boolean updateTrustAnchorsStore(String storePath, String storeName, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws TSLUpdateException
  {
    try
    {
      Log.info("Looking for cached tsl data inside " + storePath);

      boolean bNeed = needUpdate(storePath + "tl-mp.xml");

      if (bNeed) {
    	  Log.warn("tl-mp needs update: " + bNeed);
        if (download(storePath + "tl-mp.xml", "https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl-mp.xml", useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd))
        {
          Log.info("Downloaded tl-mp.xml");
        } else {
          Log.error("Unable to download tl-mp.xml");
          return false;
        }
      }

      if (!bNeed) {
        bNeed = needUpdate(storePath + "IT_TSL_signed.xml");
       
      }

      if (bNeed)
      {
    	Log.warn("IT_TSL_signed needs update: " + bNeed);
        String tsUrl = getTSUrl(storePath + "tl-mp.xml", "IT", "application/vnd.etsi.tsl+xml");
        int retry=0;
        while(retry<2){
		        if(retry==1){
		        	tsUrl = tsUrl.replace("https", "http");
		        }
		        Log.info("TS URL: " + tsUrl);
		        if (download(storePath + "IT_TSL_signed.xml", tsUrl, useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd))
		        {
		          Log.info("Downloaded IT_TSL_signed.xml");
		          retry=2;
		        } else {
		          Log.error("Unable to download IT_TSL_signed.xml");
		          if (retry==0){
		        	  retry=retry+1;
		          }else{
		        	  retry=retry+1;//anche se non servirebbe
		        	  return false;
		          }
		        }
        }

        return updateTrustAnchorsFromTSL(storePath + "IT_TSL_signed.xml", storePath + storeName);
      }

      

      if (!new File(storePath + storeName).exists()) {
        Log.warn("Store " + storePath + storeName + " does not exist, creating it...");
        return updateTrustAnchorsFromTSL(storePath + "IT_TSL_signed.xml", storePath + storeName);
      }

      return true;
    }
    catch (ParseException e)
    {
      e.printStackTrace();
    }
    catch (Exception e)
    {
      throw new TSLUpdateException(e.getMessage());
    }

    return false;
  }

  public static boolean needUpdate(String tsFilePath)
    throws ParseException, Exception
  {
    File f = new File(tsFilePath);
    
    
    if (f.exists())
    {
      DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
      domFactory.setNamespaceAware(false);
      try
      {
    	 
    	 FileInputStream fis= new FileInputStream(f);
         InputStreamReader  reader=  new InputStreamReader(fis,Charset.forName("UTF-8"));
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        InputSource is = new InputSource(reader);
     // is.setEncoding("UTF-8"); -> This line causes error! Content is not allowed in prolog
       // SAXParserFactory factory = SAXParserFactory.newInstance();
	    //  SAXParser saxParser = factory.newSAXParser();
	    //  DefaultHandler handler = new DefaultHandler();
        //saxParser.parse(is, handler);
      // System.out.println( saxParser.getProperty("/TrustServiceStatusList/SchemeInformation/NextUpdate/dateTime"));
        String strNextUpdateDateTime = "2018-01-01T01:01:01Z";
        try {
        	Document doc = builder.parse(is);
          strNextUpdateDateTime = getTextNode("/TrustServiceStatusList/SchemeInformation/NextUpdate/dateTime", doc, true);
        }catch(Exception e) {
        	Log.warn("Errore nella lettura del file "+tsFilePath);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        Date nextUpdatedate = dateFormat.parse(strNextUpdateDateTime);

        Date now = new Date();

        return now.after(nextUpdatedate);
      }
      catch (ParserConfigurationException e)
      {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }

      return true;
    }

    return true;
  }

  public static String getTSUrl(String tsFilePath, String locale, String format)
    throws ParseException, Exception
  {
    String tsUrl = null;
    File f = new File(tsFilePath);

    if (f.exists()) {
      DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
      domFactory.setNamespaceAware(false);
      try
      {
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        FileInputStream fis= new FileInputStream(f);
        InputStreamReader  reader=  new InputStreamReader(fis,Charset.forName("UTF-8"));
       //DocumentBuilder builder = domFactory.newDocumentBuilder();
       InputSource is = new InputSource(reader);
    // is.setEncoding("UTF-8"); -> This line causes error! Content is not allowed in prolog
      // SAXParserFactory factory = SAXParserFactory.newInstance();
	    //  SAXParser saxParser = factory.newSAXParser();
	    //  DefaultHandler handler = new DefaultHandler();
       //saxParser.parse(is, handler);
     // System.out.println( saxParser.getProperty("/TrustServiceStatusList/SchemeInformation/NextUpdate/dateTime"));
       Document doc = builder.parse(is);
       // Document doc = builder.parse(f);
        System.out.println("getTSUrl: "+tsFilePath+" "+locale+" "+format);
        return getTextNode("//TSLLocation[(..//OtherInformation[SchemeTerritory = '" + locale + "'] and ..//OtherInformation[MimeType = '" + format + "'])]", doc, true);
      }
      catch (ParserConfigurationException e)
      {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }

    }

    return "";
  }

  public static boolean download(String archivePath, String url, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd)
  {
    InputStream crlInputStream = null;
    InputStreamReader isr = null;
    BufferedReader in = null;
    URLConnection conn = null;
    try
    {
      URL urla = new URL(url);
      System.out.println("Use proxy: "+useProxy);
      if (useProxy) {
        Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));

        SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));

        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

				if (url.contains("https")){
					conn = (HttpsURLConnection)urla.openConnection(proxy);
					
					List<String> limited = new LinkedList<String>();
					for(String suite : ((SSLSocket)((HttpsURLConnection)conn).getSSLSocketFactory().createSocket()).getEnabledCipherSuites())
					{
					    if(!suite.contains("_DHE_"))
					    {
					        limited.add(suite);
					    }
					}
					((SSLSocket)((HttpsURLConnection)conn).getSSLSocketFactory().createSocket()).setEnabledCipherSuites(limited.toArray(
					    new String[limited.size()]));
					
				}else {
					conn = (HttpURLConnection)urla.openConnection(proxy);
				}

			} else if (url.contains("https")) {
				
				try {
		                       
		            SSLExcludeCipherConnectionHelper sslExclHelper = new SSLExcludeCipherConnectionHelper();
		           
		            conn = sslExclHelper.getConnection(urla);
		            
		        } catch (Exception ex) {
		            ex.printStackTrace();
		        }

			} else {
				conn = (HttpURLConnection)urla.openConnection();
			}

      crlInputStream = conn.getInputStream();

      isr = new InputStreamReader(crlInputStream);
      in = new BufferedReader(isr);
    }
    catch (MalformedURLException e1)
    {
      e1.printStackTrace();
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }

    if (crlInputStream != null)
    {
      File f = new File(archivePath);
      try
      {
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));
        String inputLine;
        while ((inputLine = in.readLine()) != null)
        {
          bw.write(inputLine);
        }

        in.close();
        bw.close();

        return true;
      }
      catch (FileNotFoundException e)
      {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }

    }

    return false;
  }

  private static String getTextNode(String path, Document doc, boolean nullIfEmpty)
    throws XPathExpressionException
  {
    XPathFactory factory = XPathFactory.newInstance();
    XPath xpath = factory.newXPath();

    XPathExpression expr = xpath.compile(path);
    Object obj = expr.evaluate(doc, XPathConstants.NODE);

    if (obj == null)
    {
      return null;
    }

    if ((((Node)obj).getTextContent().equals("")) && 
      (nullIfEmpty)) {
      return null;
    }

    return ((Node)obj).getTextContent();
  }

  public static String executeXpathNode(Node node, String path)
    throws XPathExpressionException
  {
    Node ret = null;

    XPathFactory factory = XPathFactory.newInstance();
    XPath xpath = factory.newXPath();

    XPathExpression expr = xpath.compile(path);
    ret = (Node)expr.evaluate(node, XPathConstants.NODE);

    if (ret != null) {
      return ret.getTextContent();
    }
    return "";
  }

  public static NodeList getNodeList(String path, Node node)
    throws XPathExpressionException
  {
    NodeList ret = null;

    XPathFactory factory = XPathFactory.newInstance();
    XPath xpath = factory.newXPath();

    XPathExpression expr = xpath.compile(path);
    ret = (NodeList)expr.evaluate(node, XPathConstants.NODESET);

    return ret;
  }

  public static boolean updateTrustAnchorsFromTSL(String sourceTSLFilePath, String destinationStorePath) {
    DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
    domFactory.setNamespaceAware(false);

    File f = new File(sourceTSLFilePath);

    HashMap certMap = new HashMap();

    BASE64Decoder decoder = new BASE64Decoder();

    int count = 0;
    try
    {
      DocumentBuilder builder = domFactory.newDocumentBuilder();
      Document doc = builder.parse(f);

      NodeList nl = getNodeList("/TrustServiceStatusList/TrustServiceProviderList//TrustServiceProvider", doc);

      if (nl != null)
      {
        Log.info("TrustServiceProviders found: " + nl.getLength());

        for (int i = 0; i < nl.getLength(); i++) {
          Node nd = nl.item(i);

          String TSPName = executeXpathNode(nd, "TSPInformation/TSPName/Name[@lang = 'IT' or @lang = 'it']");

          if (TSPName.equals("")) {
            TSPName = executeXpathNode(nd, "TSPInformation/TSPName/Name[@lang = 'en' or @lang = 'EN']");
          }

          NodeList nll = getNodeList("TSPServices//TSPService", nd);

          for (int j = 0; j < nll.getLength(); j++)
          {
            Node ndd = nll.item(j);
            String strCert = executeXpathNode(ndd, "ServiceInformation/ServiceDigitalIdentity/DigitalId/X509Certificate");

            ByteArrayInputStream bai = new ByteArrayInputStream(decoder.decodeBuffer(strCert));
            try
            {
              CertificateFactory cf = CertificateFactory.getInstance("X.509");

              Certificate cert = null;

              cert = cf.generateCertificate(bai);

              count++;

              certMap.put(String.format("%03d", new Object[] { Integer.valueOf(count) }) + '-' + TSPName + '(' + String.format("%02d", new Object[] { Integer.valueOf(j) }) + ')', cert);
            }
            catch (CertificateException e)
            {
              e.printStackTrace();
            }
          }

        }

        if (certMap.values().size() > 0) {
          File fl = new File(destinationStorePath);
          fl.delete();
          return checkCreateCAStore(destinationStorePath, null, certMap);
        }
      }

      return false;
    }
    catch (ParserConfigurationException e)
    {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    catch (SAXException e) {
      e.printStackTrace();
    }
    catch (XPathExpressionException e) {
      e.printStackTrace();
    }

    return false;
  }

  public static boolean updateTrustAnchorsFromP7M(String sourceFilePath, String destinationStorePath) throws IOException
  {
    File fl = new File("/Users/erivelli/Desktop/OK_BUONO.p7m");

    FileInputStream fis = new FileInputStream(fl);

    DataInputStream is = new DataInputStream(fis);

    byte[] zipped = P7M.extractP7M(is);

    ByteArrayInputStream bis = new ByteArrayInputStream(zipped);

    ZipInputStream zis = new ZipInputStream(bis);

    HashMap certMap = new HashMap();
    ZipEntry entry;
    while ((entry = zis.getNextEntry()) != null)
    {
      if (!entry.isDirectory())
      {
        if ((entry.getName().toLowerCase().contains(".der")) || (entry.getName().toLowerCase().contains(".cer")) || (entry.getName().toLowerCase().contains(".crt")))
        {
          Log.info("Extracting: " + entry);

          byte[] data = new byte[(int)entry.getSize()];

          zis.read(data, 0, data.length);
          try
          {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            Certificate cert = null;

            cert = cf.generateCertificate(new ByteArrayInputStream(data));

            certMap.put(entry.getName(), cert);
          }
          catch (CertificateException e)
          {
            e.printStackTrace();
          }
        }

      }

    }

    zis.close();

    checkCreateCAStore("/Users/erivelli/Desktop/cnipastore.jks", null, certMap);

    fis.close();

    return true;
  }

  private static boolean checkCreateCAStore(String storePath, String provider, HashMap<String, Certificate> certMap) {
    File file = new File(storePath);

    FileOutputStream fon = null;
    try
    {
      fon = new FileOutputStream(file);
      KeyStore k;
      if (provider == null)
        k = KeyStore.getInstance("JKS");
      else {
        k = KeyStore.getInstance("JKS", provider);
      }
      k.load(null, "chageit".toCharArray());

      Iterator myVeryOwnIterator = certMap.entrySet().iterator();

      int i = 0;

      while (myVeryOwnIterator.hasNext()) {
        i++;

        Map.Entry entry = (Map.Entry)myVeryOwnIterator.next();

        k.setCertificateEntry((String)entry.getKey(), (Certificate)entry.getValue());
      }

      k.store(fon, "changeit".toCharArray());
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    catch (CertificateException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return true;
  }

  public static boolean checkCreateCRLStore(String storePath, String provider) {
    File file = new File(storePath);

    FileOutputStream fon = null;
    try
    {
      fon = new FileOutputStream(file);
      KeyStore k;
      if (provider == null)
        k = KeyStore.getInstance("JKS");
      else {
        k = KeyStore.getInstance("JKS", provider);
      }
      k.load(null, "chageit".toCharArray());

      k.store(fon, "changeit".toCharArray());
    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }
    catch (KeyStoreException e) {
      e.printStackTrace();
    }
    catch (NoSuchProviderException e) {
      e.printStackTrace();
    }
    catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    catch (CertificateException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return true;
  }
}