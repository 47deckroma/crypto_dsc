package com.adobe.consulting.signature;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.CRLException;
import java.security.cert.CertPathBuilder;
import java.security.cert.CertPathBuilderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.PKIXCertPathBuilderResult;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1String;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.cms.Time;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.Extensions;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.asn1.x509.qualified.QCStatement;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaCertStoreBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import org.bouncycastle.cert.ocsp.OCSPReq;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.cert.selector.X509CertificateHolderSelector;
import org.bouncycastle.cert.selector.jcajce.JcaX509CertSelectorConverter;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.ocsp.RevokedStatus;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.util.encoders.Hex;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

import com.adobe.consulting.types.P7MVerifyResult;
import com.adobe.consulting.types.VerifySignatureResult;
import com.adobe.idp.Document;

public class P7M
{
	Logger Log = Logger.getLogger(P7M.class);
	protected String m_proxyHost;
	protected String m_proxyPort;
	protected String m_proxyUser;
	protected String m_proxyPassword;
	protected boolean m_useProxy = false;
	
	public void setProxyHost(String aHost)
	{
		if (aHost != null) {
			aHost = aHost.trim();
		}
		this.m_proxyHost = aHost;
	}

	public String getProxyHost()
	{
		return this.m_proxyHost;
	}

	public void setProxyPort(String aHost) {
		if (aHost != null) {
			aHost = aHost.trim();
		}
		this.m_proxyPort = aHost;
	}

	public String getProxyPort()
	{
		return this.m_proxyPort;
	}

	public void setProxyUser(String aUser)
	{
		if (aUser != null) {
			aUser = aUser.trim();
		}
		this.m_proxyUser = aUser;
	}

	public String getProxyUser()
	{
		return this.m_proxyUser;
	}

	public void setProxyPassword(String aPassword)
	{
		if (aPassword != null) {
			aPassword = aPassword.trim();
		}
		this.m_proxyPassword = aPassword;
	}

	public String getProxyPassword()
	{
		return this.m_proxyPassword;
	}

	public P7MVerifyResult verifyP7M(Document p7mDoc, String caStoreFolder, String caStoreName, boolean extractContent, ProtocolTypes useProtocol,boolean updateTrustAnchorsStore) throws VerifyException, TSLUpdateException {
		return verifyP7M(p7mDoc.getInputStream(), caStoreFolder, caStoreName, extractContent, useProtocol,updateTrustAnchorsStore);
	}

	public P7MVerifyResult verifyP7M(InputStream p7mDoc, String caStoreFolder, String caStoreName, boolean extractContent, ProtocolTypes useProtocol,boolean updateTrustAnchorsStore) throws VerifyException, TSLUpdateException {
		P7MVerifyResult result = new P7MVerifyResult();
		boolean isSigned = false;
		boolean selfSigned = false;
		Time signingTime = null;
		String ocsp = null;

		result.setVerifySignatureResult(VerifySignatureResult.P7M_VERIFY_ERROR);
		try
		{
			Security.addProvider(new BouncyCastleProvider());

			if (this.m_proxyHost != null) {
				this.m_useProxy = true;
				this.Log.info("Using Proxy: " + this.m_proxyUser + "@" + this.m_proxyHost + ":" + this.m_proxyPort);
			}
			
			
			if(updateTrustAnchorsStore){
				if (TrustedLists.updateTrustAnchorsStore(caStoreFolder, caStoreName, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
					this.Log.info("END updateTrustAnchorsStore  OK");
				} else {
					this.Log.error("updateTrustAnchorsStore: FAILED!");
					result.setExceptionMessage("updateTrustAnchorsStore: FAILED!");
					return result;
				}
			}else{
				this.Log.info("updateTrustAnchorsStore: SKIPP");
			}

			byte[] data = new byte[p7mDoc.available()];
			DataInputStream in = new DataInputStream(p7mDoc);
			try
			{
				in.readFully(data);
				in.close();
			}
			catch (IOException e) {
				e.printStackTrace();
			}

			CMSSignedData sd = null;
			try
			{
				sd = new CMSSignedData(data);
			} catch (Exception e) {
				e.printStackTrace();
				this.Log.error("P7M:CMSSignedData(data);"+e.getMessage());
				return result;
			}

			this.Log.info("Content Type: " + sd.getSignedContent().getContentType().getId());
			//System.out.println("SecProv:"+ Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()));
			//SecretKeyFactory keyFac = SecretKeyFactory.getInstance("BC");
			//System.out.println(keyFac);
			//Cipher cipher =  Cipher.getInstance("BC");
			//System.out.println(cipher);
			//security.provider =org.bouncycastle.jce.provider.BouncyCastleProvider 
			JcaCertStoreBuilder storeBuilder = new JcaCertStoreBuilder();

			storeBuilder.addCertificates(sd.getCertificates());
			storeBuilder.addCRLs(sd.getCRLs());
			storeBuilder.setProvider("BC");

			CertStore certs = storeBuilder.build();
			SignerInformationStore si = sd.getSignerInfos();
			Collection c = si.getSigners();
			this.Log.info("Found " + c.size() + " signer/s.");
			Iterator it = c.iterator();

			int iVerified = 0;

			if (it.hasNext())
			{
				selfSigned = false;

				SignerInformation signer = (SignerInformation)it.next();

				this.Log.info("Issuer: " + signer.getSID().getIssuer());

				JcaX509CertSelectorConverter conv = new JcaX509CertSelectorConverter();

				X509CertificateHolderSelector sel = new X509CertificateHolderSelector(signer.getSID().getIssuer(), signer.getSID().getSerialNumber());
				X509CertSelector signerConstraints = conv.getCertSelector(sel);

				Collection certCollection = certs.getCertificates(signerConstraints);

				Iterator certIt = certCollection.iterator();
				X509Certificate cert = (X509Certificate)certIt.next();

				X500Name name = new X500Name(cert.getSubjectDN().getName());

				this.Log.info("Certificate subject DN: " + name.toString());

				String szSigner = "";
				try{
					RDN[] rdns = name.getRDNs(BCStyle.CN);
					if (rdns.length > 0) {
						szSigner =   "CN:" + rdns[0].getFirst().getValue() ;
					}
					rdns = name.getRDNs(BCStyle.O);
					if (rdns.length > 0) {
						szSigner =  szSigner+ ", O:" + rdns[0].getFirst().getValue();
					}
					rdns = name.getRDNs(BCStyle.OU);
					if (rdns.length > 0) {
						szSigner = szSigner + ", OU:" + rdns[0].getFirst().getValue();
					}
				}catch(Exception e){
					szSigner = name.toString();
				}

				result.setSigner(szSigner);

				this.Log.info("Signer: " + result.getSigner());

				result.setSignerNotAfter(cert.getNotAfter());
				try
				{
					cert.checkValidity();

					result.setSignerCertificateTimeValid(true);

					this.Log.info("User Certificate is time valid");
				}
				catch (CertificateExpiredException e) {
					this.Log.warn("User Certificate is expired!");
				}

				if (isSelfSigned(cert)) {
					this.Log.warn("*** P7M Certificate is self-signed ***");
					selfSigned = true;
				}

				if (signer.verify(cert, "BC"))
				{
					result.setContentValid(true);

					iVerified++;
					this.Log.info("Signer [" + iVerified + "] did verify with success");

					CMSProcessableByteArray cpb = (CMSProcessableByteArray)sd.getSignedContent();

					byte[] original = (byte[])cpb.getContent();

					result.setContentValid(true);

					if (extractContent) {
						result.setExtractedDocument(new Document(original));
					}

					if (!result.isSignerCertificateTimeValid()) return result;

					this.Log.info("Got encoded pkcs7 bytes " + original.length + " bytes");

					char[] password = "changeit".toCharArray();

					KeyStore anchors = loadCacertsKeyStore(caStoreFolder + caStoreName, null);

					PKIXCertPathBuilderResult res = null;
					X509CertSelector selector = null;

					if (selfSigned) {
						try {
							anchors.setCertificateEntry(cert.getSerialNumber().toString(36), cert);
						}
						catch (KeyStoreException e) {
							result.setVerifySignatureResult(VerifySignatureResult.P7M_VERIFY_ERROR);
							result.setExceptionMessage(e.getMessage());
							return result;
						}

					}
					else
					{
						this.Log.info("Using " + anchors.size() + " trusted identities");

						selector = new X509CertSelector();
						selector.setCertificate(cert);

						PKIXBuilderParameters params = new PKIXBuilderParameters(anchors, selector);

						params.addCertStore(certs);
						params.setRevocationEnabled(false);

						Date dt = new SimpleDateFormat("dd/MM/yyyy").parse("02/08/2009");

						CertPathBuilder builder = CertPathBuilder.getInstance("PKIX", "BC");

						res = (PKIXCertPathBuilderResult)builder.build(params);

						TrustAnchor CA = res.getTrustAnchor();

						result.setAnchorNotAfter(CA.getTrustedCert().getNotAfter());

						isQualifiedCertificate(CA.getTrustedCert());

						byte[] certificateByteArray = CA.getTrustedCert().getEncoded();
						try
						{
							CA.getTrustedCert().checkValidity();

							result.setAnchorCertificateTimeValid(true);

							this.Log.info("CA Certificate is time valid");
						}
						catch (CertificateExpiredException e) {
							this.Log.warn("CA Certificate is expired!");
						}
						catch (CertificateNotYetValidException e)
						{
							this.Log.warn("CA Certificate not yet valid!");
						}

						if (!result.isAnchorCertificateTimeValid()) {
							return result;
						}

						if (isCertificateRevoked(CA.getTrustedCert(), CA.getTrustedCert(), caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
							result.setAnchorCertificateRevoked(true);
							this.Log.warn("CA Certificate is revoked!");
						} else {
							this.Log.warn("CA Certificate NOT revoked");
						}

						if (isCertificateRevoked(CA.getTrustedCert(), cert, caStoreFolder, useProtocol, this.m_useProxy, this.m_proxyHost, this.m_proxyPort, this.m_proxyUser, this.m_proxyPassword)) {
							result.setSignerCertificateRevoked(true);
							this.Log.warn("Signer Certificate is revoked!");
						} else {
							this.Log.warn("Signer Certificate NOT revoked");
						}

					}

					Object cp = res.getCertPath();

					this.Log.info("Certificate chain validated successfully -> " + cp.getClass().getName());

					result.setChainValid(true);
					result.setVerifySignatureResult(VerifySignatureResult.SUCCESS);

					return result;
				}

				result.setExceptionMessage("Signer [" + iVerified + "] did NOT verify");
				this.Log.error("Signer [" + iVerified + "] did NOT verify");
				return result;
			}

		}
		catch (CMSException e)
		{
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (NoSuchProviderException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (CertStoreException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (CertificateExpiredException e) {
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (CertificateException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (KeyStoreException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (CertPathBuilderException e) {
			this.Log.warn(e.getMessage());
			result.setExceptionMessage(e.getMessage());
		}
		catch (ParseException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (TSLUpdateException e) {
			if (this.Log.getEffectiveLevel() == Level.INFO)
				this.Log.info(e.getMessage() + " - Increase log level for more info");
			else {
				e.printStackTrace();
			}
			result.setExceptionMessage(e.getMessage());
			throw new TSLUpdateException(e.getMessage());
		}
		catch (IOException e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			throw new VerifyException(e.getMessage());
		}
		catch (GeneralSecurityException e)
		{
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
			result.setExceptionMessage(e.getMessage());
			result.setVerifySignatureResult(VerifySignatureResult.P7M_VERIFY_ERROR);
 		}
		return result;
	}

	public boolean isQualifiedCertificate(X509Certificate certificate)
	throws IOException, CertificateException
	{
		ASN1Sequence qcStatements;
		boolean existsQCStatement = false;

		this.Log.info("Checking certificate compliancy...");

		byte[] extensionValue = certificate.getExtensionValue(X509Extension.qCStatements.getId());
		if (extensionValue == null) {
			this.Log.warn("Certificate IS NOT Quality Compliant");
			return false;
		}
		try
		{
			DEROctetString oct = (DEROctetString)new ASN1InputStream(new ByteArrayInputStream(extensionValue)).readObject();
			qcStatements = (ASN1Sequence)new ASN1InputStream(oct.getOctets()).readObject();
		}
		catch (IOException e)
		{
			throw new RuntimeException("IO error: " + e.getMessage(), e);
		}

		Enumeration qcStatementEnum = qcStatements.getObjects();
		boolean qcCompliance = false;

		while (qcStatementEnum.hasMoreElements())
		{
			existsQCStatement = true;

			QCStatement qcStatement = QCStatement.getInstance(qcStatementEnum
					.nextElement());
			DERObjectIdentifier statementId = qcStatement.getStatementId();

			this.Log.info("statement Id: " + statementId.getId());

			if (QCStatement.id_etsi_qcs_QcCompliance.equals(statementId))
			{
				this.Log.info("statement Id: " + statementId.getId() + " follows compliace");

				qcCompliance = true;
			}
			else
			{
				qcCompliance = false;
				this.Log.warn("statement Id: " + statementId.getId() + " doesn't follow compliace");
				break;
			}
		}

		if (existsQCStatement) {
			if (!qcCompliance) {
				this.Log.info("Cerificate is Quality Compliant");
				return false;
			}
		}
		else this.Log.warn("Certificate IS NOT Quality Compliant");

		return true;
	}

	public boolean isCertificateRevoked(X509Certificate TrustedAnchor, X509Certificate certificate, String caStoreFolder, ProtocolTypes useProtocol, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws CertStoreException, IOException, CertificateParsingException, NoSuchProviderException {
		String OCSPResponder = "";

		Vector OCSPs = getOCSPResponder(certificate);

		if (OCSPs != null) {
			Object[] sOCSPs = OCSPs.toArray();

			OCSPResponder = (String)sOCSPs[0];

			this.Log.info("Found OCSP responder for certificate at " + OCSPResponder);
			try
			{
				OCSPReq req = generateOCSPRequest(TrustedAnchor, certificate.getSerialNumber());

				OCSPResp resp = getOCSPResponseFromHttp(req.getEncoded(), OCSPResponder, useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd);

				if (resp.getStatus() != 0)
				{
					this.Log.info("Invalid OCSP response status: " + resp.getStatus());
				}

				BasicOCSPResp basicResponse = (BasicOCSPResp)resp.getResponseObject();

				if (basicResponse != null) {
					SingleResp[] responses = basicResponse.getResponses();
					if (responses.length == 1) {
						SingleResp singleResp = responses[0];
						Object status = singleResp.getCertStatus();
						if (status == CertificateStatus.GOOD) {
							this.Log.info("OCSP Status is GOOD");
							return false;
						}
						if ((status instanceof RevokedStatus)) {
							throw new IOException("OCSP Status is revoked!");
						}

						throw new IOException("OCSP Status is unknown!");
					}

				}

				BasicOCSPResp brep = (BasicOCSPResp)resp.getResponseObject();

				JcaX509CertificateConverter conv = new JcaX509CertificateConverter();

				conv.setProvider("BC");

				boolean verify = brep.isSignatureValid((ContentVerifierProvider)conv);

				if (!verify) {
					this.Log.error("ERRORE OCSP VALIDAZIONE");
				}

				SingleResp[] singleResps = brep.getResponses();

				for (int i = 0; i < singleResps.length; i++) {
					SingleResp singleResp = singleResps[i];
					Object certStatus = singleResp.getCertStatus();

					if (certStatus != null)
					{
						this.Log.error("Status is not null. 'null' is the success result " + certStatus.toString());
					}
				}

			}
			catch (org.bouncycastle.cert.ocsp.OCSPException e1)
			{
				e1.printStackTrace();
			}
			catch (CertificateEncodingException e) {
				e.printStackTrace();
			}
			catch (OperatorCreationException e) {
				e.printStackTrace();
			}
			catch (org.bouncycastle.ocsp.OCSPException e) {
				e.printStackTrace();
			}

		}
		else
		{
			Vector urls = getCrlDistributionPoint(certificate);

			Collection crlCollection = new ArrayList();

			byte[] val = (byte[])null;
			Hashtable env = new Hashtable();

			boolean bDownloadedCERTCRL = false;

			if (urls != null) {
				Object[] objs = urls.toArray();

				for (int j = 0; j < objs.length; j++) {
					String crlURLString = ((DERIA5String)objs[j]).getString();
					this.Log.info("Found CERT CRL download endpoint: " + crlURLString);

					if (((crlURLString.contains("ldap://")) && (useProtocol == ProtocolTypes.Ldap)) || 
							((crlURLString.contains("http://")) && (useProtocol == ProtocolTypes.Http)) || 
							(useProtocol == ProtocolTypes.Both))
					{
						try
						{
							String nameCRL = crlURLString.substring(crlURLString.lastIndexOf("/")+1);
							 this.Log.info("LOAD CERT CRL: " + crlURLString + " IN " + caStoreFolder);
							    X509CRL crl = loadCRLFromPersistentStorage(certificate,caStoreFolder,nameCRL);
							 
								if(crl==null) {
									this.Log.info("Downloaded CERT CRL: " + crlURLString);
								  crl = createCRLFromUrl(crlURLString, useProxy, proxyHost, proxyPort, proxyUsr, proxyPwd);
								  bDownloadedCERTCRL = true;
								}
							Date dt = crl.getNextUpdate();
							crlCollection.add(crl);

							if (crl.isRevoked(certificate)) {
								this.Log.warn("USER CERTIFICATE REVOKED...........");
								return true;
							}
							this.Log.info("USER CERTIFICATE NOT REVOKED.......");
						}
						catch (NamingException e)
						{
							this.Log.error("Unable to download CERT CRL: " + crlURLString + " -> " + e.getExplanation()); continue;
						}
						catch ( Exception e)
						{
							this.Log.error("Unable to download CERT CRL: " + crlURLString + " -> " + e.getMessage()); continue;
						}
					}
					else
						this.Log.warn("Protocol not allowed skipping CERT CRL download: " + crlURLString);
				}
			}
			else
			{
				this.Log.warn("CRL extension not found in Certificate!");
			}

		}

		return false;
	}

	public OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
	throws org.bouncycastle.cert.ocsp.OCSPException, org.bouncycastle.ocsp.OCSPException, CertificateEncodingException, OperatorCreationException, IOException
	{
		CertificateID id = new CertificateID(new JcaDigestCalculatorProviderBuilder().setProvider("BC").build().get(CertificateID.HASH_SHA1), new X509CertificateHolder(issuerCert.getEncoded()), serialNumber);

		OCSPReqBuilder OCSPBuilder = new OCSPReqBuilder();

		OCSPBuilder.addRequest(id);

		BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());

		Extension ext = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, true, new DEROctetString(nonce.toByteArray()));
		OCSPBuilder.setRequestExtensions(new Extensions(new Extension[] { ext }));

		return OCSPBuilder.build();
	}

	private boolean isSelfSigned(X509Certificate cert)
	throws CertificateException, NoSuchAlgorithmException, NoSuchProviderException
	{
		try
		{
			PublicKey key = cert.getPublicKey();

			cert.verify(key);

			return true;
		}
		catch (SignatureException sigEx)
		{
			return false;
		}
		catch (InvalidKeyException keyEx)
		{
		}

		return false;
	}

	public KeyStore loadCacertsKeyStore(String storePath, String provider)
	{
		File file = new File(storePath);

		this.Log.info("CA ANCHORS PATH LOADED FROM: " + file.getPath());

		FileInputStream fin = null;
		try {
			fin = new FileInputStream(file);
			KeyStore k;
			if (provider == null)
				k = KeyStore.getInstance("JKS");
			else
				k = KeyStore.getInstance("JKS", provider);
			k.load(fin, null);
			return k;
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (KeyStoreException e) {
			e.printStackTrace();
		}
		catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		catch (CertificateException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if (fin != null) fin.close();  } catch (Exception localException7) {  }

		}
		return null;
	}

	private ASN1Primitive getExtensionValue(X509Certificate cert, String oid) throws IOException {
		byte[] bytes = cert.getExtensionValue(oid);
		if (bytes == null) {
			return null;
		}
		ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(bytes));
		ASN1OctetString octs = (ASN1OctetString)aIn.readObject();
		aIn = new ASN1InputStream(new ByteArrayInputStream(octs.getOctets()));
		return aIn.readObject();
	}

	public Vector getOCSPResponder(X509Certificate certificate) throws CertificateParsingException
	{
		try {
			byte[] val1 = certificate.getExtensionValue(X509Extension.authorityInfoAccess.getId());

			if (val1 == null) return null;

			ASN1InputStream oAsnInStream = new ASN1InputStream(new ByteArrayInputStream(val1));
			ASN1Primitive derObj = oAsnInStream.readObject();
			DEROctetString dos = (DEROctetString)derObj;
			byte[] val2 = dos.getOctets();
			ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(val2));
			ASN1Primitive derObj2 = oAsnInStream2.readObject();
			return getDERValue(derObj2);
		}
		catch (Exception e)
		{
			throw new CertificateParsingException(e.toString());
		}
	}

	public OCSPResp getOCSPResponseFromHttp(byte[] ocspPackage, String url, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws IOException
	{
		InputStream crlInputStream = null;
		HttpURLConnection conn = null;
		try
		{
			URL urla = new URL(url);

			if (useProxy) {
				Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));

				SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));

				Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

				conn = (HttpURLConnection)urla.openConnection(proxy);
			} else {
				conn = (HttpURLConnection)urla.openConnection();
			}

			conn.setRequestProperty("Content-Type", "application/ocsp-request");
			conn.setRequestProperty("Accept", "application/ocsp-response");
			conn.setDoOutput(true);

			OutputStream out = conn.getOutputStream();

			DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));

			dataOut.write(ocspPackage);

			dataOut.flush();
			dataOut.close();

			if (conn.getResponseCode() / 100 != 2) {
				this.Log.error("Invalid HTTP response, Code: " + conn.getResponseCode() + ", Msg: " + conn.getResponseMessage());
				return null;
			}

			InputStream in = (InputStream)conn.getContent();
			return new OCSPResp(in);
		}
		catch (MalformedURLException e1)
		{
			e1.printStackTrace();
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public Vector<ASN1String> getCrlDistributionPoint(X509Certificate certificate)
	throws CertificateParsingException, IOException
	{
		byte[] extension = certificate.getExtensionValue(X509Extension.cRLDistributionPoints.getId());

		if (extension != null)
		{
			this.Log.info("CRL Distribution Points Extension:");

			CRLDistPoint distPoints = CRLDistPoint.getInstance(X509ExtensionUtil.fromExtensionValue(extension));

			DistributionPoint[] points = distPoints.getDistributionPoints();

			Vector urls = new Vector();

			for (int i = 0; i != points.length; i++)
			{
				DistributionPoint dp = points[i];
				DistributionPointName dpn = dp.getDistributionPoint();

				GeneralNames gns = (GeneralNames)points[i].getDistributionPoint().getName();
				GeneralName[] vgn = gns.getNames();
				for (int j = 0; j < vgn.length; j++)
				{
					if (vgn[j].getTagNo() == 6)
					{
						ASN1String s = (ASN1String)vgn[j].getName();
						this.Log.info("\t" + s.getString());
						urls.add(s);
					}
				}
			}

			return urls;
		}
		return null;
	}

	public Vector getCrlIssuingDistributionPoint(X509CRL crl) throws CertificateParsingException
	{
		try {
			byte[] val1 = crl.getExtensionValue("2.5.29.28");
			ASN1InputStream oAsnInStream = new ASN1InputStream(new ByteArrayInputStream(val1));
			ASN1Primitive derObj = oAsnInStream.readObject();
			DEROctetString dos = (DEROctetString)derObj;
			byte[] val2 = dos.getOctets();
			ASN1InputStream oAsnInStream2 = new ASN1InputStream(new ByteArrayInputStream(val2));
			ASN1Primitive derObj2 = oAsnInStream2.readObject();
			return getDERValue(derObj2);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new CertificateParsingException(e.toString());
		}
	}

	private Vector getDERValue(ASN1Primitive derObj) {
		if ((derObj instanceof DERSequence)) {
			Vector ret = new Vector();
			DERSequence seq = (DERSequence)derObj;
			Enumeration en = seq.getObjects();
			while (en.hasMoreElements()) {
				ASN1Primitive nestedObj = (ASN1Primitive)en.nextElement();
				Vector appo = getDERValue(nestedObj);
				if (appo != null) {
					ret.addAll(appo);
				}
			}
			return ret;
		}

		if ((derObj instanceof DERTaggedObject)) {
			DERTaggedObject derTag = (DERTaggedObject)derObj;
			if ((derTag.isExplicit()) && (!derTag.isEmpty())) {
				ASN1Primitive nestedObj = derTag.getObject();
				Vector ret = getDERValue(nestedObj);
				return ret;
			}
			DEROctetString derOct = (DEROctetString)derTag.getObject();
			String val = new String(derOct.getOctets());
			Vector ret = new Vector();
			ret.add(val);
			return ret;
		}

		return null;
	}

	private X509CRL createCRLFromUrl(String url, boolean useProxy, String proxyHost, String proxyPort, String proxyUsr, String proxyPwd) throws NamingException {
		InputStream crlInputStream = null;
		URLConnection conn = null;

		if (url.toLowerCase().contains("ldap://"))
		{
			byte[] val = (byte[])null;

			Hashtable env = new Hashtable();

			if (useProxy) {
				System.setProperty("http.proxyHost", proxyHost);
				System.setProperty("http.proxyPort", proxyPort);

				Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));
			}

			env.put("java.naming.factory.initial", "com.sun.jndi.ldap.LdapCtxFactory");
			env.put("java.naming.provider.url", url);
			env.put("java.naming.security.authentication", "none");

			DirContext ctx = new InitialDirContext(env);

			Attributes avals = ctx.getAttributes("");

			Attribute aval = avals.get("certificateRevocationList;binary");
			val = (byte[])aval.get();

			if ((val == null) || (val.length == 0))
			{
				this.Log.error("Unable to get crl from ldap ");

				return null;
			}

			crlInputStream = new ByteArrayInputStream(val);
		}
		else
		{
			try
			{
				URL urla = new URL(url);

				if (useProxy) {
					Authenticator.setDefault(new MyAuthenticator(proxyUsr, proxyPwd));
					SocketAddress addr = new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort));
					Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
					conn = urla.openConnection(proxy);
				} else {
					conn = urla.openConnection();
				}

				crlInputStream = conn.getInputStream();
			}
			catch (MalformedURLException e1)
			{
				this.Log.error("MalformedURLException :Unable to get crl http from" + url);
			}
			catch (IOException e1) {
				this.Log.error("IOException :Unable to get crl http from" + url);

			}catch ( Exception e1) {
				this.Log.error("Exception :Unable to get crl http from" + url);

			}

		}

		if (crlInputStream == null) return null;

		CertificateFactory cf = null;
		try
		{
			cf = CertificateFactory.getInstance("X.509");
		}
		catch (CertificateException ce) {
			ce.printStackTrace();
		}
		try
		{
			X509CRL crl = (X509CRL)cf.generateCRL(crlInputStream);
			this.Log.info("<-- CRL Issuer\t--> " + crl.getIssuerDN());
			this.Log.info("<-- CRL Effective\t--> " + crl.getThisUpdate());
			this.Log.info("<-- CRL NextDate\t--> " + crl.getNextUpdate());
			crlInputStream.close();

			return crl;
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private boolean isCRLexpired(X509CRL crl)
	{
		Calendar today = Calendar.getInstance();
		try
		{
			this.Log.info("crl.getNextUpdate() = " + crl.getNextUpdate());

			if (today.getTime().after(crl.getNextUpdate()))
				return true;
		}
		catch (Exception e) {
			this.Log.error("crl expiration check failed = " + e);
			return true;
		}
		return false;
	}

	private boolean isCRLvalid(X509CRL crl, X509Certificate ca)
	{
		try
		{
			crl.verify(ca.getPublicKey());
		} catch (Exception e) {
			this.Log.error("crl.verify failed = " + e);
			return false;
		}

		return true;
	}

	public static byte[] extractP7M(InputStream p7mDoc) throws IOException
	{
		byte[] data = new byte[p7mDoc.available()];
		DataInputStream in = new DataInputStream(p7mDoc);
		try
		{
			in.readFully(data);
			in.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		try
		{
			CMSSignedData sd = new CMSSignedData(data);

			CMSProcessableByteArray cpb = (CMSProcessableByteArray)sd.getSignedContent();

			return (byte[])cpb.getContent();
		}
		catch (CMSException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private X509CRL loadCRLFromPersistentStorage(X509Certificate userCert,String aCRLCachePath,String nameCRL)
	{
		try
		{
			String filesep = System.getProperty("file.separator");
			//String aCRLCachePath = "/";
			this.Log.info("Load CRL:"+aCRLCachePath+nameCRL);

			File aStorDir = new File(aCRLCachePath);
			if (aStorDir.exists())
			{
				CertificateFactory cf = CertificateFactory.getInstance("X.509");

				//X509CRL crl = null;

				//String aFileName = aCRLCachePath + filesep + getIssuerMD5Hash(userCert) + ".crl";
				String aFileName = aCRLCachePath   + nameCRL;
				try
				{
					FileInputStream fos = new FileInputStream(aFileName);
					return (X509CRL)cf.generateCRL(fos);
				}
				catch (FileNotFoundException localFileNotFoundException)
				{
				}
				catch (CRLException localCRLException)
				{
				}
			}
		}
		/*
		catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
		{
		}
		*/
		catch (CertificateParsingException localCertificateParsingException)
		{
		}
		catch (CertificateException localCertificateException) {
		}
		catch (Exception localException) {
		}
		return null;
	}

	private String getIssuerMD5Hash(X509Certificate userCert)
	throws NoSuchAlgorithmException
	{
		byte[] xp = userCert.getIssuerX500Principal().getEncoded();

		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] md5hash = new byte[32];
		md.update(xp, 0, xp.length);
		md5hash = md.digest();
		return String.valueOf(Hex.encode(md5hash)).toUpperCase();
	}

	 
}