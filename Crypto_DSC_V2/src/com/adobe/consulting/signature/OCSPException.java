package com.adobe.consulting.signature;

public class OCSPException extends Exception
{
  private static final long serialVersionUID = 1L;

  public OCSPException()
  {
  }

  public OCSPException(String message)
  {
    super(message);
  }
}