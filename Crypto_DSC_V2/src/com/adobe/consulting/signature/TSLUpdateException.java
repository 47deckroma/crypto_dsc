package com.adobe.consulting.signature;

public class TSLUpdateException extends Exception
{
  private static final long serialVersionUID = 1L;

  public TSLUpdateException()
  {
  }

  public TSLUpdateException(String message)
  {
    super(message);
  }
}