package com.adobe.consulting.signature;

import java.io.Serializable;

public enum ProtocolTypes
  implements Serializable
{
  Http, Ldap, Both;
}