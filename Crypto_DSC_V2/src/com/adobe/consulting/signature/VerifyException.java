package com.adobe.consulting.signature;

public class VerifyException extends Exception
{
  private static final long serialVersionUID = 1L;

  public VerifyException()
  {
  }

  public VerifyException(String message)
  {
    super(message);
  }
}