package com.adobe.consulting.signature;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import org.apache.log4j.Logger;

public class MyAuthenticator extends Authenticator
{
  String username = "";
  String pwd = "";

  static Logger Log = Logger.getLogger(MyAuthenticator.class);

  public MyAuthenticator(String username, String pwd) {
    this.username = username;
    this.pwd = pwd;
  }

  protected PasswordAuthentication getPasswordAuthentication() {
    PasswordAuthentication pwdAuth = new PasswordAuthentication(this.username, this.pwd.toCharArray());
    Log.info("Autenticating to proxy server as " + pwdAuth.getUserName());
    return pwdAuth;
  }
}