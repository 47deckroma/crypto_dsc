import com.adobe.consulting.signature.PDF;
import com.adobe.consulting.signature.ProtocolTypes;
import com.adobe.consulting.signature.TSLUpdateException;
import com.adobe.consulting.signature.VerifyException;
import com.adobe.consulting.types.v2.PDFVerifyResult2;
import com.adobe.consulting.types.v2.SignatureResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;

public class testPDF
{
  public static void main(String[] args)
    throws VerifyException, org.bouncycastle.ocsp.OCSPException
  {
    PDF pdf = new PDF();

    FileInputStream fis = null;
    try
    {
      //fis = new FileInputStream(new File("/Users/erivelli/Documents/LAVORO/- CLIENTI -/INAIL/Pdf Certificati Inail/FIRMA_SISTEMA_PROD_TOMASINI.pdf"));
       fis = new FileInputStream(new File("c:/Cert/Interruzione Pozzi gallotti2.pdf"));

    }
    catch (FileNotFoundException e)
    {
      e.printStackTrace();
    }

    try
    {
      PDFVerifyResult2 result = pdf.verifyPDF2(fis, "c:/Cert/", "CAanchorsStore", ProtocolTypes.Http,false);

      System.out.println("STATUS: " + ((SignatureResult)result.getSignatures().get(0)).getExceptionMessage());
      System.out.println("STATUS: " + result );

    }
    catch (org.bouncycastle.cert.ocsp.OCSPException e)
    {
      e.printStackTrace();
    }
    catch (TSLUpdateException e) {
      e.printStackTrace();
    }
  }
}