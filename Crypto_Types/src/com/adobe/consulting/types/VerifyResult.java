package com.adobe.consulting.types;

import java.io.Serializable;
import java.util.Date;

public class VerifyResult
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private VerifySignatureResult verifySignatureResult = null;
  private boolean _signerCertificateTimeValid = false;
  private boolean _signerCertificateRevoked = false;
  private boolean _anchorCertificateTimeValid = false;
  private boolean _anchorCertificateRevoked = false;
  private boolean _chainValid = false;
  private boolean _contentValid = false;
  private String _message = null;
  private String _signer = null;
  private Date _notAnchorAfter = null;
  private Date _notSignerAfter = null;

  public VerifySignatureResult getVerifySignatureResult()
  {
    return this.verifySignatureResult;
  }

  public void setVerifySignatureResult(VerifySignatureResult verifySignatureResult) {
    this.verifySignatureResult = verifySignatureResult;
  }

  public boolean isAnchorCertificateRevoked()
  {
    return this._anchorCertificateRevoked;
  }
  public void setAnchorCertificateRevoked(boolean anchorCertificateRevoked) {
    this._anchorCertificateRevoked = anchorCertificateRevoked;
  }

  public boolean isAnchorCertificateTimeValid() {
    return this._anchorCertificateTimeValid;
  }
  public void setAnchorCertificateTimeValid(boolean anchorCertificateTimeValid) {
    this._anchorCertificateTimeValid = anchorCertificateTimeValid;
  }

  public boolean isSignerCertificateRevoked()
  {
    return this._signerCertificateRevoked;
  }
  public void setSignerCertificateRevoked(boolean signerCertificateRevoked) {
    this._signerCertificateRevoked = signerCertificateRevoked;
  }

  public boolean isSignerCertificateTimeValid() {
    return this._signerCertificateTimeValid;
  }
  public void setSignerCertificateTimeValid(boolean signerCertificateTimeValid) {
    this._signerCertificateTimeValid = signerCertificateTimeValid;
  }

  public boolean isChainValid()
  {
    return this._chainValid;
  }

  public void setChainValid(boolean chainValid) {
    this._chainValid = chainValid;
  }

  public boolean isContentValid()
  {
    return this._contentValid;
  }
  public void setContentValid(boolean contentValid) {
    this._contentValid = contentValid;
  }

  public Date getAnchorNotAfter()
  {
    return this._notAnchorAfter;
  }
  public void setAnchorNotAfter(Date notAnchorAfter) {
    this._notAnchorAfter = notAnchorAfter;
  }

  public Date getSignerNotAfter()
  {
    return this._notSignerAfter;
  }
  public void setSignerNotAfter(Date notSignerAfter) {
    this._notSignerAfter = notSignerAfter;
  }

  public String getSigner()
  {
    return this._signer;
  }
  public void setSigner(String signer) {
    this._signer = signer;
  }

  public void setExceptionMessage(String message)
  {
    this._message = message;
  }
  public String getExceptionMessage() {
    return this._message;
  }
}