package com.adobe.consulting.types;

import java.io.Serializable;

public enum VerifySignatureResult
  implements Serializable
{
  SUCCESS, 
  HASH_FAILED, 
  UNSUPPORTED_FILE, 
  PDF_VERIFY_ERROR, 
  P7M_VERIFY_ERROR;
}