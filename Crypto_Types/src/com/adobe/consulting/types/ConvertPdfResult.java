package com.adobe.consulting.types;

import com.adobe.idp.Document;

public class ConvertPdfResult implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ConversionResult conversionResult = null;
	private Document convertedDocument = null;
	private Document conversionLog = null;
	

	public ConversionResult getConversionResult() {
		return this.conversionResult;
	}

	public void setConversionResult(ConversionResult conversionResult) {
		this.conversionResult = conversionResult;
	}

	public Document getConvertedDocument() {
		return this.convertedDocument;
	}

	public void setConvertedDocument(Document convertedDocument) {
		this.convertedDocument = convertedDocument;
	}

	public Document getConversionLog() {
		return conversionLog;
	}

	public void setConversionLog(Document conversionLog) {
		this.conversionLog = conversionLog;
	} 
	
}
