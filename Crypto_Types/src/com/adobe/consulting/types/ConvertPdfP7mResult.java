package com.adobe.consulting.types;



public class ConvertPdfP7mResult extends ConvertPdfResult implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private P7MVerifyResult m_P7mVerifyResult = null;

	public P7MVerifyResult getP7mVerifyResult() {
		return this.m_P7mVerifyResult;
	}

	public void setP7mVerifyResult(P7MVerifyResult result) {
		this.m_P7mVerifyResult = result;
	}

}
