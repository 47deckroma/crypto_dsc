package com.adobe.consulting.types;

import com.adobe.idp.Document;

public class VerifyRequest implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Document inDoc = null;
	private String hash = null;
	
	public Document getInDoc() {
		return inDoc;
	}
	public void setInDoc(Document inDoc) {
		this.inDoc = inDoc;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	} 

}
