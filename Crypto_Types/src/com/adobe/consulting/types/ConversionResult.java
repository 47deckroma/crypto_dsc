package com.adobe.consulting.types;

public enum ConversionResult implements java.io.Serializable {
	SUCCESS, 
	INVALID_PDFA, 
	HASH_FAILED,
	UNSUPPORTED_FILE,
	CONVERSION_ERROR,
	P7M_VERIFY_ERROR,
	PDF_VERIFY_ERROR,
	ENCRYPTED_PDF;
}
