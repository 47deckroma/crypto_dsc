package com.adobe.consulting.types;

import java.io.Serializable;

public class PDFVerifyResult extends VerifyResult
  implements Serializable
{
  private static final long serialVersionUID = 1L;
}