package com.adobe.consulting.types;

import com.adobe.idp.Document;
import java.io.Serializable;

public class P7MVerifyResult extends VerifyResult
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private Document _extractedDocument = null;

  public void setExtractedDocument(Document extractedDocument)
  {
    this._extractedDocument = extractedDocument;
  }
  public Document getExtractedDocument() {
    return this._extractedDocument;
  }
}
