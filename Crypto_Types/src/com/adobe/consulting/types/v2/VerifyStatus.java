package com.adobe.consulting.types.v2;

public enum VerifyStatus implements java.io.Serializable {
	SUCCESS, 
	HASH_FAILED,
	UNSUPPORTED_FILE,
	PDF_VERIFY_ERROR,
	PDF_VERIFY_ALL_VALID,
	PDF_VERIFY_NOT_VALID,
	PDF_NOT_SIGNED,
	P7M_VERIFY_ERROR;
}
