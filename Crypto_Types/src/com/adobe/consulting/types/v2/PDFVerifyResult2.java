package com.adobe.consulting.types.v2;

import java.util.ArrayList;
import java.util.List;



public class PDFVerifyResult2 implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "PDFVerifyResult2 [_status=" + _status + ", _contentValid="
				+ _contentValid + ", signatures=" + signatures + ", _message="
				+ _message + "]";
	}
	
	private VerifyStatus _status = null;
	private Boolean _contentValid = null;
	
	private List<SignatureResult> signatures = new ArrayList<SignatureResult>();
	
	private String _message = null;
	
	
	//GS
	public List<SignatureResult> getSignatures() {
		return signatures;
	}

	public void setSignatures(List<SignatureResult> signatures) {
		this.signatures = signatures;
	}

	public VerifyStatus getStatus() {
		return _status;
	}

	public void setStatus(VerifyStatus status) {
		_status = status;
	}
	
	public void setExceptionMessage(String message) {
		this._message = message;
	}
	public String getExceptionMessage() {
		return _message;
	}
	
	public void setContentValid(Boolean contentValid) {
		this._contentValid = contentValid;
	}
	public Boolean getContentValid() {
		return _contentValid;
	}


}
