package com.adobe.consulting.types.v2;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class SignatureResult implements java.io.Serializable {

	@Override
	public String toString() {
		return "SignatureResult [_fieldName=" + _fieldName
				+ ", _signerCertificateTimeValid="
				+ _signerCertificateTimeValid + ", _signerCertificateRevoked="
				+ _signerCertificateRevoked + ", _anchorCertificateTimeValid="
				+ _anchorCertificateTimeValid + ", _anchorCertificateRevoked="
				+ _anchorCertificateRevoked + ", _chainValid=" + _chainValid
				+ ", _contentValid=" + _contentValid + ", _message=" + _message
				+ ", _signer=" + _signer + ", _notAnchorAfter="
				+ _notAnchorAfter + ", _notSignerAfter=" + _notSignerAfter
				+ ", _signerCertificatePolicies=" + _signerCertificatePolicies
				+ "]";
	}
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private String _fieldName = null;
	private boolean _signerCertificateTimeValid;
	private boolean _signerCertificateRevoked;
	private boolean _anchorCertificateTimeValid;
	private boolean _anchorCertificateRevoked;
	private boolean _chainValid;
	private boolean _contentValid;
	private String _message = null;
	private String _signer = null;
	private Date _notAnchorAfter = null;
	private Date _notSignerAfter = null;
	private HashMap<String, String> _signerCertificatePolicies = new HashMap<String, String>();
	
	//FIELD NAME
	public String getSignatureFieldName() {
		return _fieldName;
	}
	
	public void setSignatureFieldName(String fieldName) {
		this._fieldName = fieldName;
	}
	
	//ANCHOR
	public boolean isAnchorCertificateRevoked() {
		return _anchorCertificateRevoked;
	}
	public void setAnchorCertificateRevoked(boolean anchorCertificateRevoked) {
		_anchorCertificateRevoked = anchorCertificateRevoked;
	}
	
	public boolean isAnchorCertificateTimeValid() {
		return _anchorCertificateTimeValid;
	}
	public void setAnchorCertificateTimeValid(boolean anchorCertificateTimeValid) {
		_anchorCertificateTimeValid = anchorCertificateTimeValid;
	}
	
	
	//SIGNER
	public boolean isSignerCertificateRevoked() {
		return _signerCertificateRevoked;
	}
	public void setSignerCertificateRevoked(boolean signerCertificateRevoked) {
		_signerCertificateRevoked = signerCertificateRevoked;
	}
	
	public boolean isSignerCertificateTimeValid() {
		return _signerCertificateTimeValid;
	}
	public void setSignerCertificateTimeValid(boolean signerCertificateTimeValid) {
		_signerCertificateTimeValid = signerCertificateTimeValid;
	}
	
	//CHAIN
	public boolean isChainValid() {
		return _chainValid;
	}
	
	public void setChainValid(boolean chainValid) {
		_chainValid = chainValid;
	}
	
	//CONTENT VALID
	public boolean isContentValid() {
		return _contentValid;
	}
	public void setContentValid(boolean contentValid) {
		_contentValid = contentValid;
	}
	
	//SCADENZA ANCHOR
	public Date getAnchorNotAfter() {
		return _notAnchorAfter;
	}
	public void setAnchorNotAfter(Date notAnchorAfter) {
		_notAnchorAfter = notAnchorAfter;
	}
	
	//SCADENZA SIGNER
	public Date getSignerNotAfter() {
		return _notSignerAfter;
	}
	public void setSignerNotAfter(Date notSignerAfter) {
		_notSignerAfter = notSignerAfter;
	}
	
	//NOME SIGNER
	public String getSigner() {
		return _signer;
	}

	public void setSigner(String signer) {
		_signer = signer;
	}
	
	//SIGNER CERTIFICATE POLICIES
	public HashMap<String, String> getSignerCertificatePolicies() {
		return _signerCertificatePolicies;
	}

	public void setSignerCertificatePolicies(HashMap<String, String> SignerCertificatePolicies) {
		_signerCertificatePolicies = SignerCertificatePolicies;
	}
	
	//ECCEZZIONE
	public void setExceptionMessage(String message) {
		this._message = message;
	}
	public String getExceptionMessage() {
		return _message;
	}

}
