package com.adobe.consulting.types;

import com.adobe.idp.Document;

public class ConvertPdfRequest implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Document inDoc = null;
	private String hash = null;
	private String extension = null;
	boolean p7m = false;
	
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public Document getInDoc() {
		return inDoc;
	}
	public void setInDoc(Document inDoc) {
		this.inDoc = inDoc;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public boolean isP7m() {
		return p7m;
	}
	public void setP7m(boolean p7m) {
		this.p7m = p7m;
	}
	
}
